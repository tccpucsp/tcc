/* global Ext */

Ext.define('resources.js.tcc.usuario.formulario.FormularioController', {
    wFormulario: null,
    store: null,
    action: null,
    id: null,
    constructor: function (cfg) {
        var me = this;

        if (!Ext.isObject(cfg)) {
            cfg = {};
        }

        if (Ext.isObject(cfg.grid)) {
            cfg.grid.enable();
        }

        me.wFormulario = resources.js.tcc.utils.Util.create('resources.js.tcc.usuario.formulario.FormularioView');
        me.inicializar();

        me.action = cfg.action;
        if (me.action === 'adicionar') {
            me.configurarAdicionarForm();
        } else if (me.action === 'modificar') {
            me.id = cfg.id;
            me.configurarModificarForm();
        }
        if (Ext.isObject(cfg.store)) {
            me.store = cfg.store;
        }

        me.wFormulario.show();
    },
    inicializar: function () {
        var me = this;

        var bSalvar = me.wFormulario.queryById('bSalvar');
        var bCancelar = me.wFormulario.queryById('bCancelar');

        var pgSenha = me.wFormulario.queryById('pgSenha');
        var tPass = me.wFormulario.queryById('tPass');

        tPass.on('change', function (field, newValue, oldValue) {
            if (field.isValid() && !Ext.isEmpty(newValue)) {
                var index = me.aplicarPolicitaSenhas(field, pgSenha);

                switch (index) {
                    case 0:
                        pgSenha.updateProgress(0.16, 'Muito Fraca', true);
                        pgSenha.getEl().child(".x-progress-bar", true).style.backgroundColor = "#FF0000";
                        break;
                    case 1:
                        pgSenha.updateProgress(0.32, 'Fraca', true);
                        pgSenha.getEl().child(".x-progress-bar", true).style.backgroundColor = "#FF6600";
                        break;
                    case 2:
                        pgSenha.updateProgress(0.58, 'Média', true);
                        pgSenha.getEl().child(".x-progress-bar", true).style.backgroundColor = "#FFFF00";
                        break;
                    case 3:
                        pgSenha.updateProgress(0.74, 'Forte', true);
                        pgSenha.getEl().child(".x-progress-bar", true).style.backgroundColor = "#99FF00";
                        break;
                    case 4:
                        pgSenha.updateProgress(0.99, 'Muito Forte', true);
                        pgSenha.getEl().child(".x-progress-bar", true).style.backgroundColor = "#00FF00";
                        break;
                }

            } else {
                pgSenha.reset();
                pgSenha.updateText('');
            }
        }, me);

        bSalvar.on('click', function () {
            me.salvar(bSalvar);
        }, me);

        bCancelar.on('click', function () {
            me.wFormulario.close();
        }, me);

    },
    salvar: function (bSalvar) {
        var me = this;
        var formulario = me.wFormulario.queryById('fFormulario');

        if (formulario.getForm().isValid()) {
            bSalvar.disable();
            formulario.getForm().submit({
                waitMsg: 'Aguarde. Realizando operação...',
                method: 'POST',
                url: 'usuario/persistir',
                success: function (form, action) {
                    var result = Ext.decode(action.response.responseText);
                    bSalvar.enable();
                    resources.js.tcc.utils.Util.renderizarResponseMessages(action.response);
                    if (result.success) {
                        if (Ext.isObject(me.store)) {
                            me.store.reload();
                        }
                        me.wFormulario.close();
                    }
                },
                failure: function (form, action) {
                    bSalvar.enable();
                    resources.js.tcc.utils.Util.renderizarResponseMessages(action.response);
                }
            });
        }
    },
    aplicarPolicitaSenhas: function (tPass) {
        var v = tPass.getValue();

        var lw = /[a-z]{1,}/;
        var up = /[A-Z]{1,}/;
        var num = /[0-9]{1,}/;
        var pc = /[\[\],.;:?/<>@#$%&*()+!=]{1,}/;

        //Muito Baixa//
        var r = 0;
        if (v.length >= 6 && v.match(lw) && v.match(up) && v.match(num) && v.match(pc)) {
            //Muito Alta//
            r = 4;
        } else if (v.length >= 6 &&
                (v.match(lw) || v.match(up)) &&
                (v.match(num) || v.match(pc))) {
            //Alta//
            r = 3;
        } else if (v.length >= 6 ||
                (v.length >= 4 &&
                        (v.match(lw) || v.match(up)) &&
                        (v.match(pc) || v.match(num))
                        )) {
            //Media//
            r = 2;
        } else if (v.length >= 4 &&
                (v.match(lw) || v.match(up) || v.match(num) || v.match(pc))) {
            //Baixa//
            r = 1;
        }

        return r;

    },
    configurarAdicionarForm: function () {
        var me = this;

        me.wFormulario.iconCls = 'iconAdd';
        me.wFormulario.queryById('tPass').allowBlank = false;
    },
    configurarModificarForm: function () {
        var me = this;

        var formulario = me.wFormulario.queryById('fFormulario');
        formulario.disable();

        me.wFormulario.queryById('hId').setValue(me.id);

        formulario.getForm().load({
            method: 'GET',
            url: 'usuario/obter',
            params: {
                id: me.id
            },
            success: function (form, action) {
                var result = action.result;
                if (result.success) {
                    formulario.enable();
                }
            },
            failure: function (form, action) {
                resources.js.tcc.utils.Util.renderizarResponseMessages(action.response);
                me.wFormulario.queryById('bSalvar').disable();
            }
        });
    }
});