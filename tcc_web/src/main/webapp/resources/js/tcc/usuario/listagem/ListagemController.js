/* global Ext, TCC */

Ext.define('resources.js.tcc.usuario.listagem.ListagemController', {
    wListagem: null,
    constructor: function () {
        var me = this;

        var conteudo = TCC.getApplication().mainViewport.queryById('conteudo');

        var view = Ext.create('resources.js.tcc.usuario.listagem.ListagemView');

        //limpando container//
        conteudo.removeAll();

        //Inicializando//
        me.wListagem = view;
        me.inicializar();

        //renderizando//
        conteudo.add(view);

    },
    inicializar: function () {
        var me = this;

        var bListagemAdicionar = me.wListagem.queryById('bAdicionar');
        var bListagemModificar = me.wListagem.queryById('bModificar');
        var bListagemRemover = me.wListagem.queryById('bRemover');

        var sListagem = me.wListagem.getStore();

        sListagem.load();

        bListagemAdicionar.on('click', function () {
            Ext.create('resources.js.tcc.usuario.formulario.FormularioController', {
                action: 'adicionar',
                store: sListagem
            });
        }, me);

        bListagemModificar.on('click', function () {

            var validacao = resources.js.tcc.utils.Util.tratarSelecaoGrid(me.wListagem, 'modificar');
            if (validacao.result) {
                var id = validacao.selection[0].data.id;
                me.modificarRegistro(id, sListagem, me.wListagem);
            }
        }, me);

        me.wListagem.on('celldblclick', function (view, td, index, record) {
            me.modificarRegistro(record.data.id, sListagem, me.wListagem);
        }, me);

        bListagemRemover.on('click', function () {
            var validacao = resources.js.tcc.utils.Util.tratarSelecaoGrid(me.wListagem, 'remover');

            if (validacao.result) {
                var strArrIds = {};
                strArrIds.ids = [];
                for (var i = 0; i < validacao.count; i++) {
                    strArrIds.ids[i] = validacao.selection[i].data.id;
                }

                Ext.Msg.show({
                    title: 'Confirmação',
                    msg: 'Deseja realmente remover o(s) registro(s) selecionado(s)?',
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.Msg.QUESTION,
                    fn: function (btn) {
                        if (btn === 'yes') {
                            me.wListagem.disable();
                            Ext.Ajax.request({
                                url: 'usuario/remover',
                                params: strArrIds,
                                callback: function (action, success, response) {
                                    var result = Ext.decode(response.responseText);
                                    me.wListagem.enable();
                                    resources.js.tcc.utils.Util.renderizarResponseMessages(response);
                                    if (result.success) {
                                        sListagem.reload();
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }, me);

    },
    modificarRegistro: function (id, store, grid) {
        grid.disable();
        Ext.create('resources.js.tcc.usuario.formulario.FormularioController', {
            action: 'modificar',
            store: store,
            id: id,
            grid: grid
        });
    }
});
