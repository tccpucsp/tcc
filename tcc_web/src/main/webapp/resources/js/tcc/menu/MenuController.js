/* global Ext, TCC */

Ext.define('resources.js.tcc.menu.MenuController', {
    extend: 'Ext.app.Controller',
    loginController: null,
    constructor: function () {
        this.init();
    },
    init: function () {

        var app = TCC.getApplication();
        app.control({
            //Eventos de click nos menu da aplicação//
            "container[itemId=fMenuDinamico] toolbar button": {
                click: this.menuClick
            },
            "container[itemId=fMenuDinamico] toolbar menuitem": {
                click: this.menuClick
            }
        });
    },
    menuClick: function (item) {
        this.menuController.renderizaMenu(item.action);
    },
    renderizaMenu: function (action) {
        var me = this;

        switch (action) {

            case 'perfilClientes':
                Ext.create('resources.js.tcc.relatorios.perfilclientes.listagem.ListagemController');
                break;

            case 'perfilDispositivos':
                Ext.create('resources.js.tcc.relatorios.perfildispositivos.listagem.ListagemController');
                break;

            case 'aniversariantes':
                Ext.create('resources.js.tcc.relatorios.aniversariantes.listagem.ListagemController');
                break;

            case 'acessoanalitico':
                Ext.create('resources.js.tcc.relatorios.acessoanalitico.listagem.ListagemController');
                break;

            case 'gerenciarUsuarios':
                Ext.create('resources.js.tcc.usuario.listagem.ListagemController');
                break;

            case 'gerenciarRobos':
                Ext.create('resources.js.tcc.gerenciadordeprocessos.listagem.ListagemController');
                break;

            case 'logout':
                me.loginController.doLogout();
                break;
        }
    }
});
