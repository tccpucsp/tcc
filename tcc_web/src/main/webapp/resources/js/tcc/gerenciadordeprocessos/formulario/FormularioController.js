/* global Ext */

Ext.define('resources.js.tcc.gerenciadordeprocessos.formulario.FormularioController', {
    wFormulario: null,
    store: null,
    dados: null,
    action: null,
    constructor: function (cfg) {
        var me = this;

        if (!Ext.isObject(cfg)) {
            cfg = {};
        }

        me.wFormulario = resources.js.tcc.utils.Util.create('resources.js.tcc.gerenciadordeprocessos.formulario.FormularioView');

        me.action = cfg.action;
        if (me.action === 'modificar') {
            me.dados = cfg.dados;
            me.configurarModificarForm();
        }
        if (Ext.isObject(cfg.sListagem)) {
            me.store = cfg.sListagem;
        }

        me.inicializar();
        me.wFormulario.show();
    },
    inicializar: function () {
        var me = this;

//        var bSalvar = me.wFormulario.queryById('bSalvar');
        var bCancelar = me.wFormulario.queryById('bCancelar');

        var gExecucoes = me.wFormulario.queryById('gExecucoes');
        var gLogs = me.wFormulario.queryById('gLogs');

        gExecucoes.on('select', function () {
            var validacao = resources.js.tcc.utils.Util.tratarSelecaoGrid(gExecucoes, 'modificar');

            if (validacao.result) {
                var dados = validacao.selection[0];

                gLogs.getStore().getProxy().setExtraParam('execucaoId', dados.data.id);
                gLogs.getStore().reload();

            }
        }, me);

//        bSalvar.on('click', function () {
//            var formulario = me.wFormulario.queryById('fFormulario');
//            if (formulario.getForm().isValid()) {
//                bSalvar.disable();
//                formulario.getForm().submit({
//                    waitMsg: 'Aguarde. Realizando operação...',
//                    method: 'POST',
//                    url: 'gerenciadorprocesso/atualizar',
//                    success: function (form, action) {
//                        var result = Ext.decode(action.response.responseText);
//                        bSalvar.enable();
//                        resources.js.tcc.utils.Util.renderizarResponseMessages(action.response);
//                        if (result.success) {
//                            if (Ext.isObject(me.store)) {
//                                me.store.reload();
//                            }
//                            if (me.action === 'modificar') {
//                                var id = result.data.ferramentaId;
//                                me.dados = {};
//                                me.dados.data = result.data;
//                                me.wFormulario.queryById('tGrids').enable();
//                                me.wFormulario.queryById('hId').setValue(id);
//                                gExecucoes.getStore().getProxy().setExtraParam('processoId', id);
//                                gLogs.getStore().getProxy().setExtraParam('processoId', id);
//                                me.wFormulario.iconCls = 'iconEdit';
//                            }
//                            me.wFormulario.close();
//                        }
//                    },
//                    failure: function (form, action) {
//                        bSalvar.enable();
//                        resources.js.tcc.utils.Util.renderizarResponseMessages(action.response);
//                    }
//                });
//            }
//        }, me);

        bCancelar.on('click', function () {
            me.wFormulario.close();
        }, me);
    },
    configurarModificarForm: function () {
        var me = this;
        var id = me.dados.data.id;

        var formulario = me.wFormulario.queryById('fFormulario');
        var execucoesStore = me.wFormulario.queryById('gExecucoes').getStore();
        var logsStore = me.wFormulario.queryById('gLogs').getStore();
        formulario.disable();

        me.wFormulario.queryById('hId').setValue(id);
        formulario.getForm().load({
            method: 'GET',
            url: 'gerenciadorprocesso/obter',
            params: {
                id: id
            },
            success: function (form, action) {
                var result = Ext.decode(action.response.responseText);

                if (!Ext.isEmpty(result.data.dataInicio)) {
                    var fDataInicio = me.wFormulario.queryById('fDataInicio');
                    fDataInicio.setValue(Ext.Date.format(new Date(result.data.dataInicio), 'd/m/Y'));
                }

            }
        });

        execucoesStore.getProxy().setExtraParam('processoId', me.dados.get('id'));
        execucoesStore.load();
        logsStore.getProxy().setExtraParam('processoId', me.dados.get('id'));
        logsStore.load();

        formulario.enable();
    }
});
