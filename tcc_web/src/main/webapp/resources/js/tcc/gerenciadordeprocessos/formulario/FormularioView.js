/* global Ext */

Ext.define('resources.js.tcc.gerenciadordeprocessos.formulario.FormularioView', {
    extend: 'Ext.window.Window',
    requires: [
        'resources.js.tcc.gerenciadordeprocessos.ProcessoExecucaoLogModel',
        'resources.js.tcc.gerenciadordeprocessos.ProcessoExecucaoModel'
    ],
    header: {style: 'background-color: #0277bd;'},
    height: 500,
    width: 800,
    modal: true,
    layout: {
        type: 'fit'
    },
    title: 'Gerenciador de Processos',
    iconCls: 'iconRobo',
    initComponent: function () {
        var me = this;

        var execucoesStore = Ext.create('Ext.data.JsonStore', {
            proxy: {
                type: 'ajax',
                url: 'gerenciadorprocesso/listarexecucoes'
            },
            model: 'resources.js.tcc.gerenciadordeprocessos.ProcessoExecucaoModel'
        });

        var logsStore = Ext.create('Ext.data.JsonStore', {
            proxy: {
                type: 'ajax',
                url: 'gerenciadorprocesso/listarlogs'
            },
            model: 'resources.js.tcc.gerenciadordeprocessos.ProcessoExecucaoLogModel'
        });

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    itemId: 'fFormulario',
                    bodyPadding: 10,
                    border: false,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'container',
                            margin: '0 0 10 0',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                labelAlign: 'right',
                                labelWidth: 70
                            },
                            items: [
                                {
                                    xtype: 'hiddenfield',
                                    name: 'id',
                                    itemId: 'hId'
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'codigo',
                                    fieldLabel: 'Código',
                                    readOnly: true,
                                    submitValue: false,
                                    allowBlank: false,
                                    enforceMaxLength: true,
                                    maxLength: 100,
                                    flex: 1
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'descricao',
                                    readOnly: true,
                                    submitValue: false,
                                    fieldLabel: 'Descrição',
                                    allowBlank: false,
                                    enforceMaxLength: true,
                                    maxLength: 100,
                                    flex: 3
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            margin: '0 0 10 0',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                labelAlign: 'right',
                                labelWidth: 70
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    name: 'periodicidade',
                                    fieldLabel: 'Periodicidade',
                                    allowBlank: false,
                                    labelWidth: 90,
                                    readOnly: true,
                                    submitValue: false,
                                    enforceMaxLength: true,
                                    maxLength: 100,
                                    flex: 2
                                },
                                {
                                    xtype: 'datefield',
                                    fieldLabel: 'Data Inicio',
                                    name: 'dataInicio',
                                    itemId: 'fDataInicio',
                                    enforceMaxLength: true,
                                    readOnly: true,
                                    submitValue: false,
                                    maxLength: 100,
                                    flex: 2
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            margin: '0 0 10 0',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                labelAlign: 'right',
                                labelWidth: 70
                            },
                            items: [
                                {
                                    xtype: 'checkboxfield',
                                    name: 'ativo',
                                    boxLabel: 'Ativo',
                                    inputValue: true,
                                    id: 'cAtivo',
                                    enforceMaxLength: true,
                                    maxLength: 100,
                                    flex: 4
                                }
                            ]
                        },
                        {
                            xtype: 'tabpanel',
                            flex: 1,
                            itemId: 'tGrids',
                            activeTab: 0,
                            border: true,
                            items: [
                                {
                                    xtype: 'gridpanel',
                                    selModel: Ext.create('Ext.selection.CheckboxModel', {
                                    }),
                                    title: 'Execuções',
                                    itemId: 'gExecucoes',
                                    store: execucoesStore,
                                    columns: [
                                        {
                                            xtype: 'gridcolumn',
                                            dataIndex: 'id',
                                            text: 'ID',
                                            flex: 1
                                        },
                                        {
                                            xtype: 'datecolumn',
                                            dataIndex: 'dataInicio',
                                            format: 'd/m/Y H:i:s',
                                            text: 'Data Inicio',
                                            flex: 2
                                        },
                                        {
                                            xtype: 'datecolumn',
                                            dataIndex: 'dataFinal',
                                            format: 'd/m/Y H:i:s',
                                            text: 'Data Final',
                                            flex: 2
                                        },
                                        {
                                            xtype: 'gridcolumn',
                                            dataIndex: 'situacao',
                                            text: 'Situação',
                                            flex: 3
                                        }
                                    ],
                                    dockedItems: [
                                        {
                                            xtype: 'toolbar',
                                            dock: 'bottom',
                                            items: [
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'gridpanel',
                                    selModel: Ext.create('Ext.selection.CheckboxModel', {
                                    }),
                                    title: 'Logs',
                                    itemId: 'gLogs',
                                    store: logsStore,
                                    columns: [
                                        {
                                            xtype: 'gridcolumn',
                                            dataIndex: 'id',
                                            text: 'ID',
                                            flex: 1
                                        },
                                        {
                                            xtype: 'gridcolumn',
                                            dataIndex: 'log',
                                            text: 'Log',
                                            flex: 3
                                        },
                                        {
                                            xtype: 'datecolumn',
                                            dataIndex: 'data',
                                            format: 'd/m/Y H:i:s',
                                            text: 'Data',
                                            flex: 2
                                        }
                                    ],
                                    dockedItems: [
                                        {
                                            xtype: 'toolbar',
                                            dock: 'bottom',
                                            items: [
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    ui: 'footer',
                    layout: {
                        pack: 'center',
                        type: 'hbox'
                    },
                    items: [
//                        {
//                            xtype: 'button',
//                            text: 'Salvar',
//                            itemId: 'bSalvar',
//                            iconCls: 'iconSave'
//                        },
                        {
                            xtype: 'button',
                            text: 'Sair',
                            itemId: 'bCancelar',
                            iconCls: 'iconLogout'
                        }
                    ]
                }
            ],
            tools: [
                {
                    xtype: 'tool',
                    type: 'gear',
                    action: 'comment',
                    tooltip: 'Comentar'
                }
            ]
        });

        me.callParent(arguments);
    }
});
