/* global Ext */

Ext.define('resources.js.tcc.gerenciadordeprocessos.listagem.ListagemView', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gerenciadordeprocessoslistagemlistagemview',
    requires: [
        'resources.js.tcc.gerenciadordeprocessos.ProcessoModel'
    ],
    header: {style: 'background-color: #0277bd;'},
    
    border: true,
    layout: {
        type: 'fit'
    },
    title: 'Gerenciador de Processos',
    iconCls: 'iconRobo',
    initComponent: function () {
        var me = this;

        var myStore = Ext.create('Ext.data.JsonStore', {
            proxy: {
                type: 'ajax',
                url: 'gerenciadorprocesso/listarprocessos',
                reader: {
                    type: 'json'
                }
            },
            model: 'resources.js.tcc.gerenciadordeprocessos.ProcessoModel'
        });

        me.store = myStore;
        me.selModel = Ext.create('Ext.selection.CheckboxModel', {
        });

        var tbStyle = resources.js.tcc.utils.Util.getTbFullStyle();

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'codigo',
                    text: 'Código',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'descricao',
                    text: 'Descrição',
                    flex: 5
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'ativo',
                    text: 'Ativo',
                    flex: 1
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    style: tbStyle,
                    items: [
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            text: 'Detalhes',
                            itemId: 'bDetalhes',
                            iconCls: 'iconDetails'
                        },
                        {
                            xtype: 'button',
                            text: 'Executar',
                            itemId: 'bExecutar',
                            iconCls: 'iconPlay'
                        },
                        {
                            xtype: 'button',
                            text: 'Parar',
                            itemId: 'bParar',
                            iconCls: 'iconStop'
                        },
                        {
                            xtype: 'button',
                            text: 'Cancelar',
                            itemId: 'bCancelar',
                            iconCls: 'iconRemove'
                        },
                        {
                            xtype: 'tbseparator'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }
});
