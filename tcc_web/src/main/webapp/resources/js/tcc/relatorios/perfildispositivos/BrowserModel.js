/* global Ext */

Ext.define('resources.js.tcc.relatorios.perfildispositivos.BrowserModel', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'browser'
        },
        {
            name: 'total'
        }
    ]
});
