/* global Ext */

Ext.define('resources.js.tcc.relatorios.aniversariantes.listagem.PesquisarView', {
    extend: 'Ext.window.Window',
    closeAction: 'hide',
    header: {style: 'background-color: #0277bd;'},
    height: 160,
    width: 600,
    modal: true,
    layout: {
        type: 'fit'
    },
    title: 'Pesquisar',
    iconCls: 'iconFilter',
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    border: false,
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'numberfield',
                            anchor: '100%',
                            fieldLabel: 'Dia',
                            labelWidth: 100,
                            name: 'diaNasc',
                            value: Ext.Date.format(new Date(), 'd'),
                            minValue: 1,
                            maxValue: 31
                        },
                        {
                            xtype: 'numberfield',
                            anchor: '100%',
                            fieldLabel: 'Mês',
                            labelWidth: 100,
                            name: 'mesNasc',
                            value: Ext.Date.format(new Date(), 'm'),
                            allowBlank: false,
                            minValue: 1,
                            maxValue: 12
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    ui: 'footer',
                    layout: {
                        pack: 'center',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Confirmar',
                            itemId: 'bConfirmar',
                            iconCls: 'iconConfirm'
                        },
                        {
                            xtype: 'button',
                            text: 'Sair',
                            itemId: 'bCancelar',
                            iconCls: 'iconSair'
                        },
                        {
                            xtype: 'button',
                            text: 'Limpar',
                            itemId: 'bLimpar',
                            iconCls: 'iconBroom'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }
});