/* global Ext, TCC */

Ext.define('resources.js.tcc.relatorios.perfilclientes.listagem.ListagemController', {
    wListagem: null,
    constructor: function () {
        var me = this;

        var conteudo = TCC.getApplication().mainViewport.queryById('conteudo');

        var view = Ext.create('resources.js.tcc.relatorios.perfilclientes.listagem.ListagemView');

        //limpando container//
        conteudo.removeAll();

        //Inicializando//
        me.wListagem = view;
        me.inicializar();

        //renderizando//
        conteudo.add(view);

    },
    inicializar: function () {
        var me = this;

        var sChartAmigo = me.wListagem.queryById('chartAmigo').getStore();
        var sChartTL = me.wListagem.queryById('chartTL').getStore();
        var sChartGenero = me.wListagem.queryById('chartGenero').getStore();
        var sChartFE = me.wListagem.queryById('chartFE').getStore();


        var chartGenero = me.wListagem.queryById('chartGenero');

        sChartGenero.on('load', function (store, records) {
            var total = 0;
            for (var i = 0; i < records.length; i++) {
                total += records[i].get('total');
            }
            chartGenero.axes.items[0].maximum = total;
        }, me);


        sChartAmigo.load();
        sChartTL.load();
        sChartGenero.load();
        sChartFE.load();

    }
});
