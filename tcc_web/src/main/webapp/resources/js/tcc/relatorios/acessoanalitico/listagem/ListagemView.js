/* global Ext */

Ext.define('resources.js.tcc.relatorios.acessoanalitico.listagem.ListagemView', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.relatoriosacessoanaliticolistagemview',
    requires: [
        'resources.js.tcc.relatorios.acessoanalitico.AAModel'
    ],
    header: {style: 'background-color: #0277bd;'},
    
    layout: {
        type: 'fit'
    },
    title: 'Acessos - Analítico',
    iconCls: 'iconAccess',
    itemId: 'gListagem',
    border: true,
    initComponent: function () {
        var me = this;

        var myStore = Ext.create('Ext.data.JsonStore', {
            model: 'resources.js.tcc.relatorios.acessoanalitico.AAModel',
            proxy: {
                type: 'ajax',
                url: 'relatorio/acessosAnalitico',
                reader: {
                    type: 'json'
                }
            }
        });

        me.store = myStore;

        var tbStyle = resources.js.tcc.utils.Util.getTbFullStyle();

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'ap_mac_ssid',
                    text: 'SSID',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'device_mac',
                    text: 'MAC',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'ip',
                    text: 'IP',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'meraki_id',
                    text: 'Meraki ID',
                    flex: 1
                },
                {
                    xtype: 'datecolumn',
                    flex: 1,
                    dataIndex: 'start',
                    text: 'Start'
//                    format: 'd/m/Y H:i'
                },
                {
                    xtype: 'datecolumn',
                    dataIndex: 'stop',
                    text: 'Stop',
                    flex: 1
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    style: tbStyle,
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            text: 'Pesquisar',
                            itemId: 'bPesquisar',
                            iconCls: 'iconFilter'
                        },
                        {
                            xtype: 'tbseparator'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }
});