/* global Ext, TCC */

Ext.define('resources.js.tcc.relatorios.perfildispositivos.listagem.ListagemController', {
    wListagem: null,
    constructor: function () {
        var me = this;

        var conteudo = TCC.getApplication().mainViewport.queryById('conteudo');

        var view = Ext.create('resources.js.tcc.relatorios.perfildispositivos.listagem.ListagemView');

        //limpando container//
        conteudo.removeAll();

        //Inicializando//
        me.wListagem = view;
        me.inicializar();

        //renderizando//
        conteudo.add(view);

    },
    inicializar: function () {
        var me = this;

        var sChartSO = me.wListagem.queryById('chartSO').getStore();
        var sChartBrowser = me.wListagem.queryById('chartBrowser').getStore();
        var sChartResolution = me.wListagem.queryById('chartResolution').getStore();
        var sChartTipo = me.wListagem.queryById('chartTipo').getStore();


        var chartTipo = me.wListagem.queryById('chartTipo');

        sChartTipo.on('load', function (store, records) {
            var total = 0;
            for (var i = 0; i < records.length; i++) {
                total += records[i].get('total');
            }
            chartTipo.axes.items[0].maximum = total;
        }, me);


        sChartSO.load();
        sChartBrowser.load();
        sChartResolution.load();
        sChartTipo.load();

    }
});
