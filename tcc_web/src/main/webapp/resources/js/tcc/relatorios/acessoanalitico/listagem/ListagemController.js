/* global Ext, TCC */

Ext.define('resources.js.tcc.relatorios.acessoanalitico.listagem.ListagemController', {
    wListagem: null,
    wPesquisar: null,
    constructor: function () {
        var me = this;

        var conteudo = TCC.getApplication().mainViewport.queryById('conteudo');

        var view = Ext.create('resources.js.tcc.relatorios.acessoanalitico.listagem.ListagemView');

        me.wPesquisar = resources.js.tcc.utils.Util.create('resources.js.tcc.relatorios.acessoanalitico.listagem.PesquisarView');

        //limpando container//
        conteudo.removeAll();

        //Inicializando//
        me.wListagem = view;
        me.inicializar();

        //renderizando//
        conteudo.add(view);

    },
    inicializar: function () {
        var me = this;

        var bListagemPesquisar = me.wListagem.queryById('bPesquisar');

        var bPesquisarConfirmar = me.wPesquisar.queryById('bConfirmar');
        var bPesquisarCancelar = me.wPesquisar.queryById('bCancelar');
        var bPesquisarLimpar = me.wPesquisar.queryById('bLimpar');

        var fPesquisar = me.wPesquisar.down('form');

        var sListagem = me.wListagem.getStore();

        sListagem.load();

        bListagemPesquisar.on('click', function () {
            me.wPesquisar.show();
        }, me);

        bPesquisarLimpar.on('click', function () {
            fPesquisar.getForm().reset();
            sListagem.proxy.extraParams = {};
            sListagem.reload();
            me.wPesquisar.close();
        }, me);

        bPesquisarCancelar.on('click', function () {
            me.wPesquisar.close();
        }, me);

        bPesquisarConfirmar.on('click', function () {

            var dataAcesso = fPesquisar.down('datefield[name=dataAcesso]').getValue();

            if (dataAcesso !== "") {

                sListagem.proxy.extraParams = {
                    dataAcesso: Ext.Date.format(new Date(dataAcesso), 'Y/m/d')
                };

                sListagem.reload();
                me.wPesquisar.close();
            } else {
                Ext.Msg.alert('Atenção!', 'Preencha o campo do formulário!');
            }

        }, me);

        me.wListagem.on('close', function () {
            me.wPesquisar.closeAction = 'destroy';
            me.wPesquisar.close();
        }, me);

    }
});
