/* global Ext */

Ext.define('resources.js.tcc.relatorios.aniversariantes.listagem.ListagemView', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.relatoriosaniversarianteslistagemview',
    requires: [
        'resources.js.tcc.relatorios.aniversariantes.AModel'
    ],
    header: {style: 'background-color: #0277bd;'},
    layout: {
        type: 'fit'
    },
    title: 'Aniversáriantes',
    iconCls: 'iconGift',
    itemId: 'gListagem',
    border: true,
    initComponent: function () {
        var me = this;

        var myStore = Ext.create('Ext.data.JsonStore', {
            model: 'resources.js.tcc.relatorios.aniversariantes.AModel',
            proxy: {
                type: 'ajax',
                url: 'relatorio/aniversariantes',
                reader: {
                    type: 'json'
                }
            }
        });

        me.store = myStore;

        var tbStyle = resources.js.tcc.utils.Util.getTbFullStyle();

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'name',
                    text: 'Nome',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'last_name',
                    text: 'Sobrenome',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'email',
                    text: 'E-mail',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'gender',
                    text: 'Gênero',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'phone',
                    text: 'Fone',
                    flex: 1
                },
                {
                    xtype: 'datecolumn',
                    flex: 1,
                    dataIndex: 'birthday',
                    text: 'Data Nascimento'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'idade',
                    text: 'Idade',
                    flex: 1
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    style: tbStyle,
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            text: 'Pesquisar',
                            itemId: 'bPesquisar',
                            iconCls: 'iconFilter'
                        },
                        {
                            xtype: 'tbseparator'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }
});