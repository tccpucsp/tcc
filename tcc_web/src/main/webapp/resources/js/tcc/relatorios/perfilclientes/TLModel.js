/* global Ext */

Ext.define('resources.js.tcc.relatorios.perfilclientes.TLModel', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'tipoLogin'
        },
        {
            name: 'total'
        }
    ]
});
