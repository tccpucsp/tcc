/* global Ext */

Ext.define('resources.js.tcc.relatorios.acessoanalitico.listagem.PesquisarView', {
    extend: 'Ext.window.Window',
    closeAction: 'hide',
    height: 140,
    header: {style: 'background-color: #0277bd;'},
    width: 600,
    modal: true,
    layout: {
        type: 'fit'
    },
    title: 'Pesquisar',
    iconCls: 'iconFilter',
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    border: false,
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'datefield',
                            anchor: '100%',
                            fieldLabel: 'Data Acesso',
                            labelWidth: 100,
                            name: 'dataAcesso',
                            allowBlank: false
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    ui: 'footer',
                    layout: {
                        pack: 'center',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Confirmar',
                            itemId: 'bConfirmar',
                            iconCls: 'iconConfirm'
                        },
                        {
                            xtype: 'button',
                            text: 'Sair',
                            itemId: 'bCancelar',
                            iconCls: 'iconSair'
                        },
                        {
                            xtype: 'button',
                            text: 'Limpar',
                            itemId: 'bLimpar',
                            iconCls: 'iconBroom'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }
});