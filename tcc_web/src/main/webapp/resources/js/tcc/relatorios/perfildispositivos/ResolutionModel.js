/* global Ext */

Ext.define('resources.js.tcc.relatorios.perfildispositivos.ResolutionModel', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'resolution'
        },
        {
            name: 'total'
        }
    ]
});
