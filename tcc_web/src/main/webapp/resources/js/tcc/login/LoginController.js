/* global Ext, TCC */

Ext.define('resources.js.tcc.login.LoginController', {
    extend: 'Ext.app.Controller',
    fFormularioLogin: null,
    fFormularioPassword: null,
    menuController: null,
    loginUsuario: null,
    nomeUsuario: null,
    constructor: function (cfg) {
        var me = this;

        if (!Ext.isObject(cfg)) {
            cfg = {};
        }

        if (Ext.isObject(cfg.menuController)) {
            me.menuController = cfg.menuController;
            me.menuController.loginController = me;
        }

        this.init();
    },
    init: function () {
        var app = TCC.getApplication();
        app.control({
            "mainviewport": {
                afterrender: this.onViewportAfterRender
            },
            "loginformularioformularioform toolbar button[action=login]": {
                click: this.doLogin
            },
            "loginformularioformulariosenhaform toolbar button[action=createNewPassword]": {
                click: this.gerarNovaSenha
            }
        });
    },
    mensagem: function (texto) {
        Ext.Msg.show({
            title: 'Aviso',
            msg: texto,
            buttons: Ext.Msg.OK,
            icon: Ext.Msg.WARNING
        });
    },
    onViewportAfterRender: function (abstractcomponent) {
        var me = this.loginController;
        var viewport = abstractcomponent;

        me.fFormularioLogin = Ext.create('resources.js.tcc.login.formulario.FormularioForm', {
            renderTo: 'login'
        });
        me.fFormularioLogin.show();

        Ext.addBehaviors({
            '#esqueciMinhaSenha@click': function () {

                var windowQuery = Ext.ComponentQuery.query('loginformularioformulariosenhaform');

                if (windowQuery.length === 0) {
                    me.fFormularioPassword = Ext.create('resources.js.tcc.login.formulario.FormularioSenhaForm', {
                        renderTo: 'createNewPassword'
                    });
                    me.fFormularioPassword.show();

                } else { // Se ela ja existe, joga ela pra frente //
                    windowQuery[0].toFront();
                }

            }
        });
        viewport.getLayout().setActiveItem(1);
        Ext.Ajax.request({
            url: 'auth/logged',
            method: 'POST',
            callback: function (action, success, response) {
                var result = Ext.decode(response.responseText);
                if (success) {
                    var logado = false;
                    if (result.success) {
                        logado = true;
                    }
                    if (logado) {
                        me.nomeUsuario = result.nomeUsuario;
                        me.buildMenu(viewport);
                        viewport.getLayout().setActiveItem(2);

                    } else {
                        viewport.getLayout().setActiveItem(1);
                    }
                } else {
                    viewport.getLayout().setActiveItem(1);
                }
            }
        });
    },
    gerarNovaSenha: function (button) {
        var form = button.up('window').down('form');

        if (form.getForm().isValid()) {
            form.getForm().submit({
                method: 'POST',
                reset: true,
                success: function (form, action) {
                    resources.js.tcc.utils.Util.renderizarResponseMessages(action.response);
                    button.up('window').close();
                },
                failure: function (form, action) {
                    resources.js.tcc.utils.Util.renderizarResponseMessages(action.response);
                }
            });
        }
    },
    doLogout: function () {
        var me = this;
        var viewport = TCC.getApplication().mainViewport;

        Ext.Ajax.request({
            url: 'exc',
            success: function () {
                //limpando container
                viewport.queryById('conteudo').removeAll();

            },
            failure: function () {
                Ext.Msg.alert('Erro!', 'Falha no processo de logout.');
            }
        });

        //voltando para tela de login//
        viewport.getLayout().setActiveItem(1);
    },
    doLogin: function () {
        var me = this.loginController;
        var viewport = this.mainViewport;

        if (me.fFormularioLogin.getForm().isValid()) {
            me.fFormularioLogin.getForm().submit({
                waitMsg: 'Logando ...',
                method: 'POST',
                reset: true,
                success: function (form, action) {
                    if (action.result.success) {
                        var login = me.fFormularioLogin.down('textfield[name=j_username]').getValue();
                        var result = Ext.decode(action.response.responseText);
                        me.loginUsuario = login;

                        me.nomeUsuario = result.nomeUsuario;
                        me.buildMenu(viewport);
                        viewport.queryById('conteudo').show();
                        viewport.getLayout().setActiveItem(2);
                    }
                },
                failure: function (form, action) {
                    resources.js.tcc.utils.Util.renderizarResponseMessages(action.response);
                }
            });
        }
    },
    submitForm: function (formulario, param) {
        var me = this;
        var viewport = TCC.getApplication().mainViewport;

        formulario.getForm().submit({
            waitMsg: 'Logando ...',
            method: 'POST',
            reset: true,
            params: param,
            success: function (form, action) {
                var result = Ext.decode(action.response.responseText);

                var login = me.fFormularioLogin.down('textfield[name=j_username]').getValue();
                me.loginUsuario = login;

                /*Limpando a lista de permissões de combobox*/
                me.comboboxController.clearList();
                me.sessionHash = action.result.sessionHash;
                me.buildMenu(viewport);
                viewport.queryById('conteudo').show();
                viewport.getLayout().setActiveItem(2);
                //disparando conexão ativa//
                me.checkAtivo = true;
                me.check();
            },
            failure: function (form, action) {
                resources.js.tcc.utils.Util.renderizarResponseMessages(action.response);
            }
        });
    },
    construirMenu: function () {
        var me = this;
        var viewport = TCC.getApplication().mainViewport;



    },
    buildMenu: function (viewport) {
        var me = this;
        var mainContainer = viewport.queryById('main');

        var menuContainer = Ext.create('Ext.container.Container', {
            itemId: 'menuWest',
            region: 'west',
            layout: {
                type: 'vbox'
            }
        });

        var logo = Ext.create('Ext.Img', {
            src: 'resources/imagens/logo.png',
            maxWidth: '250',
            maxHeight: '175'
        });

        var fMenu = Ext.create('Ext.form.Panel', {
            itemId: 'fMenuDinamico',
            header: {style: 'background-color: #0277bd;'},
            title: 'Bem vindo(a) ' + me.nomeUsuario,
            flex: 1,
            width: 250,
            padding: 5,
            layout: {
                type: 'fit'
            },
            items: [
                {
                    xtype: 'toolbar',
                    ui: 'footer',
                    style: 'background-color: #989898;',
                    itemId: 'tbMenuDinamico',
                    vertical: true,
                    defaults: {
                        width: '220px'
                    },
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'iconRelatorio',
                            name: 'relatorios',
                            itemId: 'bRelatorios',
                            text: 'Relatórios',
                            menu: {
                                xtype: 'menu',
                                items: [
                                    {
                                        xtype: 'menuitem',
                                        iconCls: 'iconProfile',
                                        action: 'perfilClientes',
                                        itemId: 'bPerfilClientes',
                                        text: 'Perfil dos Clientes'
                                    },
                                    {
                                        xtype: 'menuitem',
                                        iconCls: 'iconDispositivo',
                                        action: 'perfilDispositivos',
                                        itemId: 'bPerfilDispositivos',
                                        text: 'Perfil dos Dispositivos'
                                    },
                                    {
                                        xtype: 'menuitem',
                                        iconCls: 'iconGift',
                                        action: 'aniversariantes',
                                        itemId: 'bAniversariantes',
                                        text: 'Aniversariantes'
                                    },
                                    {
                                        xtype: 'menuitem',
                                        iconCls: 'iconAccess',
                                        action: 'acessoanalitico',
                                        itemId: 'bAcessoAnalitico',
                                        text: 'Acessos - Analítico'
                                    }
                                ]
                            }
                        },
                        {
                            xtype: 'button',
                            iconCls: 'iconRobo',
                            action: 'gerenciarRobos',
                            name: 'gerenciarRobos',
                            itemId: 'bGerenciarRobos',
                            text: 'Gerenciar Processos'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'iconUsers',
                            action: 'gerenciarUsuarios',
                            name: 'gerenciarUsuarios',
                            itemId: 'bGerenciarUsuarios',
                            text: 'Gerenciar Usuários'
                        },
                        {
                            xtype: 'button',
                            name: 'logout',
                            itemId: 'bLogout',
                            action: 'logout',
                            iconCls: 'iconLogout',
                            text: 'Logout'
                        }
                    ]
                }
            ]
        });

        menuContainer.add(logo);
        menuContainer.add(fMenu);

        mainContainer.add(menuContainer);
        me.construirMenu();
    }
});