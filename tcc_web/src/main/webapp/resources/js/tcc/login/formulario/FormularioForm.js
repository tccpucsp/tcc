/* global Ext */

Ext.define('resources.js.tcc.login.formulario.FormularioForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.loginformularioformularioform',
    height: 145,
    width: 350,
    bodyPadding: 10,
    title: 'Autenticação',
    overlapHeader: true,
    url: 'auth',
    border: true,
    initComponent: function () {
        var me = this;

        var tbStyle = resources.js.tcc.utils.Util.getTbFullStyle();

        me.initialConfig = Ext.apply({
            url: 'auth'
        }, me.initialConfig);

        Ext.applyIf(me, {
            defaults: {
                anchor: '100%',
                labelWidth: 60,
                labelAlign: 'right'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'j_username',
                    fieldLabel: 'Usuário',
                    allowBlank: false,
                    enableKeyEvents: true,
                    listeners: {
                        keydown: {
                            fn: me.logar,
                            scope: me
                        }
                    }
                },
                {
                    xtype: 'textfield',
                    inputType: 'password',
                    name: 'j_password',
                    fieldLabel: 'Senha',
                    margin: '0px 0px 50px 0px ',
                    allowBlank: false,
                    enableKeyEvents: true,
                    listeners: {
                        keydown: {
                            fn: me.logar,
                            scope: me
                        }
                    }
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    style: tbStyle,
                    dock: 'bottom',
                    layout: {
                        pack: 'center',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'button',
                            action: 'login',
                            iconCls: 'iconLogin',
                            text: 'Login'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    },
    logar: function (textfield, e) {
        var button = textfield.up('form').down('button[action=login]');
        if (e.getKey() === e.ENTER) {
            button.fireEvent('click');
        }
    }
});


