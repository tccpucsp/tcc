/* global Ext, TCC */
//TCC = variavel global do projeto
//Ext = variavel global do framework
Ext.define('resources.js.tcc.utils.Util', {
    statics: {
        renderizarResponseMessages: function (response, functionResponse) {

            if (!Ext.isEmpty(response)) {

                var result = Ext.decode(response.responseText);
                var renderizar = true;
                if (!Ext.isEmpty(result) && Ext.isDefined(result.success)) {
                    if (Ext.isArray(result.tccErrors) && result.tccErrors.length > 0) {
                        var errorMessage = "";
                        for (var i = 0; i < result.tccErrors.length; i++) {
                            errorMessage += result.tccErrors[i].message + "</br>";
                        }
                        if (renderizar) {
                            Ext.Msg.show({
                                title: 'Erro',
                                msg: errorMessage,
                                buttons: Ext.Msg.OK,
                                icon: Ext.Msg.ERROR
                            });
                        }
                    } else if (Ext.isArray(result.tccWarnings) && result.tccWarnings.length > 0) {
                        var warningMessage = "";
                        for (var i = 0; i < result.tccWarnings.length; i++) {
                            warningMessage += result.tccWarnings[i].message + "</br>";
                        }
                        if (renderizar) {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: warningMessage,
                                buttons: Ext.Msg.OK,
                                icon: Ext.Msg.WARNING,
                                fn: functionResponse
                            });
                        }
                    } else if (Ext.isArray(result.tccInfos) && result.tccInfos.length > 0) {
                        var infoMessage = "";
                        for (var i = 0; i < result.tccInfos.length; i++) {
                            infoMessage += result.tccInfos[i].message + "</br>";
                        }
                        if (renderizar) {
                            Ext.Msg.show({
                                title: 'Informação',
                                msg: infoMessage,
                                buttons: Ext.Msg.OK,
                                icon: Ext.Msg.INFO
                            });
                        }
                    } else if (!Ext.isEmpty(result.message)) {
                        var infoMessage = "";
                        infoMessage = result.message;
                        if (renderizar) {
                            Ext.Msg.show({
                                title: 'Informação',
                                msg: infoMessage,
                                buttons: Ext.Msg.OK,
                                icon: Ext.Msg.INFO
                            });
                        }
                    }
                } else {
                    if (response.status <= 0) {
                        Ext.Msg.show({
                            title: 'Erro - 0',
                            msg: 'Não foi possível realizar comunicação com o servidor.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.Msg.ERROR
                        });
                    } else if (response.status !== 200) {
                        Ext.Msg.show({
                            title: 'Erro - ' + response.status + ' ' + response.statusText,
                            msg: response.responseText,
                            buttons: Ext.Msg.OK,
                            icon: Ext.Msg.ERROR
                        });
                    }
                }
            }
        },
        renderizarWindow: function (className, alias) {
            var conteudo = TCC.getApplication().mainViewport.queryById('main');

            var constrainRules = {
                constrainTo: conteudo.getEl(),
                constrainHeader: true
            };

            //Verifica se a window já existe//
            var windowQuery = Ext.ComponentQuery.query(alias);

            if (windowQuery.length === 0) {
                var win = Ext.create(className, constrainRules);
                conteudo.add(win);
                //Buscando e exibindo a window//
                return win;

            } else { // Se ela ja existe, joga ela pra frente //
                windowQuery[0].toFront();
                return null;
            }
        },
        tratarSelecaoGrid: function (grid, tipo) {

            var response = {};
            response.result = false;
            response.selection = null;
            response.count = null;

            if (Ext.isObject(grid)) {

                var sm = grid.getSelectionModel();
                var selection = sm.getSelection();
                var count = sm.getCount();

                if (tipo === 'modificar') {

                    if (count <= 0) {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: 'Nenhum registro selecionado para a operação.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.Msg.WARNING
                        });
                    } else if (count > 1) {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: 'Selecione apenas um registro para a operação.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.Msg.WARNING
                        });
                    } else {

                        response.result = true;
                        response.selection = selection;
                        response.count = count;
                    }

                } else if (tipo === 'remover') {

                    if (count <= 0) {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: 'Nenhum registro selecionado para a operação.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.Msg.WARNING
                        });
                    } else {

                        response.result = true;
                        response.selection = selection;
                        response.count = count;
                    }
                } else {
                    Ext.MessageBox.show({
                        title: 'Mensagem',
                        msg: 'Erro na função tratarSelecaoGrid(utilities). ' +
                                'Tipo de seleção do Grid inválida!',
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.ERROR
                    });
                }
            } else {
                Ext.MessageBox.show({
                    title: 'Mensagem',
                    msg: 'Erro na função tratarSelecaoGrid(utilities). ' +
                            'Grid Inválido!',
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                });
            }
            return response;
        },
        create: function (className, store, selModel) {
            var conteudo = TCC.getApplication().mainViewport.queryById('main');

            var constrainRules = {
                constrainTo: conteudo.getEl(),
                constrainHeader: true
            };
            if (!Ext.isEmpty(store)) {
                constrainRules.store = store;
            }
            if (!Ext.isEmpty(selModel)) {
                constrainRules.selModel = selModel;
            }

            return Ext.create(className, constrainRules);
        },
        messageBoxToFront: function () {
            var queryResult = Ext.ComponentQuery.query('messagebox');
            if (!Ext.isEmpty(queryResult)) {
                queryResult[0].toFront();
            }
        },
        /**
         * Recarregando combobox paginado para a primeira pagina
         * @param {Ext.form.ComboBox} combo
         * @returns {void}
         */
        reloadCombo: function (combo) {
            if (Ext.isObject(combo)) {
                combo.getPicker().down('pagingtoolbar').moveFirst();
            }
        },
        /**
         * Formatações de layout
         * @returns {String}
         */
        getTbStyle: function () {
            return 'border-bottom-style: solid; border-bottom-width: 1px; border-bottom-color: #3399CC; border-left-style: solid; border-left-width: 1px; border-left-color: #3399CC; border-right-style: solid; border-right-width: 1px; border-right-color: #3399CC;';
        },
        /**
         * Formatações de layout
         * @returns {String}
         */
        getTbBottomLessStyle: function () {
            return 'border-left-style: solid; border-left-width: 1px; border-left-color: #3399CC; border-right-style: solid; border-right-width: 1px; border-right-color: #3399CC;';
        },
        /**
         * Formatações de layout
         * @returns {String}
         */
        getTbFullStyle: function () {
            return 'border-bottom-style: solid; border-bottom-width: 1px; border-bottom-color: #3399CC; border-left-style: solid; border-left-width: 1px; border-left-color: #3399CC; border-right-style: solid; border-right-width: 1px; border-right-color: #3399CC; border-top-style: solid; border-top-width: 1px; border-top-color: #3399CC;';
        },
        /**
         * Formatações de layout
         * @returns {String}
         */
        getGridStyle: function () {
            return 'border-bottom-style: solid; border-bottom-width: 1px; border-bottom-color: #3399CC;';
        },
        /**
         * @description Substitui todas as ocorrencias do token na string pelo replacement.
         * @param {String} string String em que o método irá ocorrer.
         * @param {String} token Valor que será substituído.
         * @param {String} replacement valor que substituirá o token na string.
         * @return {String} Nova string com os valores substituídos.
         */
        replaceAll: function (string, token, replacement) {
            if (Ext.isEmpty(string) || Ext.isEmpty(token) || !Ext.isDefined(replacement)) {
                return '';
            }
            var retorno = string;
            var index;
            do {
                index = retorno.indexOf(token);

                retorno = retorno.replace(token, replacement);

            } while (index !== -1);

            return retorno;
        }
    }
});