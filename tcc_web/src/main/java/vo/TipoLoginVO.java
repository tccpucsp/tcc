package vo;

public class TipoLoginVO {

    private String tipoLogin;
    private Long total;

    public TipoLoginVO() {
    }

    public TipoLoginVO(String tipoLogin, Long total) {
        this.tipoLogin = tipoLogin;
        this.total = total;
    }

    public String getTipoLogin() {
        return tipoLogin;
    }

    public void setTipoLogin(String tipoLogin) {
        this.tipoLogin = tipoLogin;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

}
