package vo;

import com.tcc.entity.ProcessoExecucao;
import java.util.Date;

public class ProcessoExecucaoVO {

    private String id;
    private String processo;
    private String situacao;
    private Date dataInicio;
    private Date dataFinal;

    public ProcessoExecucaoVO() {
    }

    public ProcessoExecucaoVO(ProcessoExecucao pe) {
        this.id = pe.get_id();
        this.processo = pe.getProcesso() != null ? pe.getProcesso().getDescricao() : null;
        this.situacao = pe.getSituacao();
        this.dataInicio = pe.getDataInicio();
        this.dataFinal = pe.getDataFinal();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProcesso() {
        return processo;
    }

    public void setProcesso(String processo) {
        this.processo = processo;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

}
