package vo;

import com.tcc.entity.Usuario;

public class UsuarioVO {

    private String id;
    private String username;
    private String name;
    private String email;
    private String password;

    public UsuarioVO() {
    }

    public UsuarioVO(Usuario u) {
        this.id = u.get_id();
        this.username = u.getUsername();
        this.name = u.getName();
        this.email = u.getEmail();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
