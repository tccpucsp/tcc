package vo;

import com.tcc.entity.Session;
import java.util.Date;

public class AcessosAnalitocoVO {

    private String id;
    private String device_mac;
    private String meraki_id;
    private String ip;
    private String ap_mac_ssid;
    private Date start;
    private Date stop;

    public AcessosAnalitocoVO() {
    }

    public AcessosAnalitocoVO(Session s) {
        this.id = s.get_id();
        this.device_mac = s.getDevice_mac();
        this.meraki_id = s.getMeraki_id();
        this.ip = s.getIp();
        this.ap_mac_ssid = s.getAp_mac_ssid();
        this.start = s.getStart();
        this.stop = s.getStop();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDevice_mac() {
        return device_mac;
    }

    public void setDevice_mac(String device_mac) {
        this.device_mac = device_mac;
    }

    public String getMeraki_id() {
        return meraki_id;
    }

    public void setMeraki_id(String meraki_id) {
        this.meraki_id = meraki_id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getAp_mac_ssid() {
        return ap_mac_ssid;
    }

    public void setAp_mac_ssid(String ap_mac_ssid) {
        this.ap_mac_ssid = ap_mac_ssid;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getStop() {
        return stop;
    }

    public void setStop(Date stop) {
        this.stop = stop;
    }

}
