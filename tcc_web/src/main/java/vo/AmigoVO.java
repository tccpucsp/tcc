package vo;

public class AmigoVO {

    private String qtdAmigos;
    private Long total;

    public AmigoVO() {
    }

    public AmigoVO(String qtdAmigos, Long total) {
        this.qtdAmigos = qtdAmigos;
        this.total = total;
    }

    public String getQtdAmigos() {
        return qtdAmigos;
    }

    public void setQtdAmigos(String qtdAmigos) {
        this.qtdAmigos = qtdAmigos;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

}
