package vo;

public class FaixaEtariaVO {

    private String idade;
    private Long total;

    public FaixaEtariaVO() {
    }

    public FaixaEtariaVO(String idade, Long total) {
        this.idade = idade;
        this.total = total;
    }

    public String getIdade() {
        return idade;
    }

    public void setIdade(String idade) {
        this.idade = idade;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

}
