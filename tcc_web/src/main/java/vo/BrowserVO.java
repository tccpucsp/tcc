package vo;

public class BrowserVO {

    private String browser;
    private Long total;

    public BrowserVO() {
    }

    public BrowserVO(String browser, Long total) {
        this.browser = browser;
        this.total = total;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

}
