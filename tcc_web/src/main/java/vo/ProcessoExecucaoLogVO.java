package vo;

import com.tcc.entity.ProcessoExecucaoLog;
import java.util.Date;

public class ProcessoExecucaoLogVO {

    private String id;
    private String execucao;
    private String log;
    private Date data;

    public ProcessoExecucaoLogVO() {
    }

    public ProcessoExecucaoLogVO(ProcessoExecucaoLog pel) {
        this.id = pel.get_id();
        this.execucao = pel.getExecucao() != null ? pel.getExecucao().get_id() : null;
        this.log = pel.getLog();
        this.data = pel.getData();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExecucao() {
        return execucao;
    }

    public void setExecucao(String execucao) {
        this.execucao = execucao;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

}
