package vo;

import com.tcc.entity.Processo;
import java.util.Date;

public class ProcessoVO {

    private String id;
    private String codigo;
    private String descricao;
    private String peridiocidade;
    private Boolean ativo;
    private Date dataInicio;

    public ProcessoVO() {
    }

    public ProcessoVO(Processo p) {
        this.id = p.get_id();
        this.codigo = p.getCodigo();
        this.descricao = p.getDescricao();
        this.peridiocidade = p.getPeriodicidade();
        this.ativo = p.getAtivo();
        this.dataInicio = p.getDataInicio();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getPeridiocidade() {
        return peridiocidade;
    }

    public void setPeridiocidade(String peridiocidade) {
        this.peridiocidade = peridiocidade;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

}
