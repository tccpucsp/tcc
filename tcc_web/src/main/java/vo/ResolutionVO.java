package vo;

public class ResolutionVO {

    private String resolution;
    private Long total;

    public ResolutionVO() {
    }

    public ResolutionVO(String resolution, Long total) {
        this.resolution = resolution;
        this.total = total;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

}
