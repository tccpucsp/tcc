package vo;

public class TipoVO {

    private String tipo;
    private Long total;

    public TipoVO() {
    }

    public TipoVO(String tipo, Long total) {
        this.tipo = tipo;
        this.total = total;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

}
