package vo;

import com.tcc.entity.Device;

public class DeviceVO {

    private String mac;
    private String name;
    private String type;
    private String resolution;
    private String os;
    private String browser;
    private Boolean isMobile;

    public DeviceVO() {
    }

    public DeviceVO(Device d) {
        this.mac = d.getMac();
        this.name = d.getName();
        this.type = d.getType();
        this.resolution = d.getResolution();
        this.os = d.getOs();
        this.browser = d.getBrowser();
        this.isMobile = d.getIs_mobile();
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public Boolean getIsMobile() {
        return isMobile;
    }

    public void setIsMobile(Boolean isMobile) {
        this.isMobile = isMobile;
    }

}
