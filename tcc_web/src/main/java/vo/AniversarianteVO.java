package vo;

import com.tcc.entity.Cliente;
import java.util.Date;

public class AniversarianteVO {

    private String id;
    private String email;
    private String name;
    private String last_name;
    private Boolean gender;
    private String phone;
    private Date birthday;

    public AniversarianteVO() {
    }

    public AniversarianteVO(Cliente c) {
        this.id = c.get_id();
        this.email = c.getEmail();
        this.name = c.getName();
        this.last_name = c.getLast_name();
        this.gender = c.getGender();
        this.phone = c.getPhone();
        this.birthday = c.getBirthday();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

}
