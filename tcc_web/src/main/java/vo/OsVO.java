package vo;

public class OsVO {

    private String os;
    private Long total;

    public OsVO() {
    }

    public OsVO(String os, Long total) {
        this.os = os;
        this.total = total;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

}
