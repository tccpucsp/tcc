package com.tcc.web.auth;

import java.io.Serializable;
import java.util.Date;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AutenticacaoSO implements Serializable {

    private String usuarioLogadoId;
    private Date dataLogin;
    private String usuarioNome;

    public AutenticacaoSO() {
    }

    public void invalidar() {
        setUsuarioLogadoId(null);
        setDataLogin(null);
        setUsuarioNome(null);
    }

    public String getUsuarioLogadoId() {
        return usuarioLogadoId;
    }

    public void setUsuarioLogadoId(String usuarioLogadoId) {
        this.usuarioLogadoId = usuarioLogadoId;
    }

    public Date getDataLogin() {
        return dataLogin;
    }

    public void setDataLogin(Date dataLogin) {
        this.dataLogin = dataLogin;
    }

    public String getUsuarioNome() {
        return usuarioNome;
    }

    public void setUsuarioNome(String usuarioNome) {
        this.usuarioNome = usuarioNome;
    }

}
