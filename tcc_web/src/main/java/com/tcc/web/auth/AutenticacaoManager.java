package com.tcc.web.auth;

import com.tcc.entity.Usuario;
import com.tcc.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AutenticacaoManager {

    @Autowired
    private AutenticacaoSO autenticacaoSO;
    @Autowired
    private UsuarioService usuarioService;

    public AutenticacaoManager() {
    }

    public Usuario getUsuarioLogado() {

        return usuarioService.getById(autenticacaoSO.getUsuarioLogadoId());
    }
}
