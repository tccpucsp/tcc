package com.tcc.dao;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.tcc.entity.Agregado;
import com.tcc.entity.Cliente;
import com.tcc.entity.GenericEntity;
import com.tcc.entity.Session;
import com.tcc.entity.Usuario;
import com.tcc.utils.converters.DateConverter;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activity.InvalidActivityException;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;
import vo.AmigoVO;
import vo.BrowserVO;
import vo.FaixaEtariaVO;
import vo.GeneroVO;
import vo.OsVO;
import vo.ResolutionVO;
import vo.TipoLoginVO;
import vo.TipoVO;

@Component
public class MongoDbDAO extends GenericDAO {

    private MongoClient mongoClient;
    private List<ServerAddress> servidores;
    private List<MongoCredential> credenciais;
    private String dbName;
    private String collectionName;

    public MongoDbDAO() {
        super();

        this.servidores = new ArrayList<>();
        this.servidores.add(new ServerAddress("localhost"));
        this.credenciais = null;
        this.dbName = "tcc";
        this.collectionName = null;
        this.connect();
    }

    @Override
    public <T extends GenericEntity, E extends Agregado> Boolean insert(Object entity, Class<T> entityType,
            Class<E> agregadoType) {

        try {

            //Obtendo objeto do MongoDatabase//
            MongoDatabase db = this.mongoClient.getDatabase(this.dbName);

            //Obtendo objeto da Table//
            MongoCollection<Document> coll = db.getCollection(entityType.getSimpleName().toLowerCase());

            //Configuracao politica de escrita//
            this.mongoClient.setWriteConcern(WriteConcern.JOURNALED);

            //Construindo objeto de Documento com a estrutura do Banco MongoDB
            Document doc = new Document();

            //Percorrendo atrributos da classe
            BeanInfo beanInfo = Introspector.getBeanInfo(entityType);
            for (PropertyDescriptor propertyDesc : beanInfo.getPropertyDescriptors()) {
                String propertyName = propertyDesc.getName();
                Object value = propertyDesc.getReadMethod().invoke(entity);

                //Populando objeto de Documento//
                if (propertyName != null && !propertyName.isEmpty() && !"_id".equals(propertyName)
                        && !"class".equals(propertyName) && value != null) {

                    if (value instanceof Agregado && agregadoType != null) {

                        E agregadoObject = agregadoType.newInstance();
                        doc.append(propertyName, agregadoObject.serializar());

                    } else {
                        doc.append(propertyName, value.toString());
                    }
                }

            }

            //Inserindo///
            coll.insertOne(doc);
        } catch (InstantiationException | IntrospectionException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {

            StringBuilder message = new StringBuilder();
            message.append("Erro no banco de dados: ").append(e.getMessage());

            System.out.println(message.toString());
            return false;
        }

        return true;
    }

    @Override
    public <T extends GenericEntity, E extends Agregado> Boolean update(Object entity, Class<T> entityType,
            Class<E> agregadoType) {

        try {

            //Obtendo objeto do MongoDatabase//
            MongoDatabase db = this.mongoClient.getDatabase(this.dbName);

            //Obtendo objeto da Table//
            MongoCollection<Document> coll = db.getCollection(entityType.getSimpleName().toLowerCase());

            //Configuracao politica de escrita//
            this.mongoClient.setWriteConcern(WriteConcern.JOURNALED);

            //Construindo objeto de Documento com a estrutura do Banco MongoDB
            Document doc = new Document();

            String id = null;

            //Percorrendo atrributos da classe
            BeanInfo beanInfo = Introspector.getBeanInfo(entityType);
            for (PropertyDescriptor propertyDesc : beanInfo.getPropertyDescriptors()) {
                String propertyName = propertyDesc.getName();
                Object value = propertyDesc.getReadMethod().invoke(entity);

                //obtendo ID do registro//
                if (propertyName != null && "_id".equals(propertyName) && value != null) {
                    id = value.toString();
                }

                //Populando objeto de equivalencia//
                if (propertyName != null && !propertyName.isEmpty() && !"class".equals(propertyName)
                        && !"_id".equals(propertyName) && value != null) {

                    //Caso atributo seja uma classe agregada//
                    if (value instanceof Agregado && agregadoType != null) {

                        E agregadoObject = agregadoType.newInstance();
                        agregadoObject.fullConstruct(value);

                        doc.append(propertyName, agregadoObject.serializar());

                    } else {
                        doc.append(propertyName, value.toString());
                    }
                }

            }
            if (id == null) {
                throw new IllegalArgumentException("Registro não possui um ID válido");
            }

            //Objeto do filtro WHERE do update//
            BasicDBObject query = new BasicDBObject("_id", new ObjectId(id));

            //Atualizando///
            coll.updateOne(query, new Document("$set", doc));

        } catch (InstantiationException | IntrospectionException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {

            StringBuilder message = new StringBuilder();
            message.append("Erro no banco de dados: ").append(e.getMessage());

            System.out.println(message.toString());
            return false;
        }

        return true;
    }

    @Override
    public <T extends GenericEntity> Boolean delete(Object entity, Class<T> entityType) {

        try {

            //Obtendo objeto do MongoDatabase//
            MongoDatabase db = this.mongoClient.getDatabase(this.dbName);

            //Obtendo objeto da Table//
            MongoCollection<Document> coll = db.getCollection(entityType.getSimpleName().toLowerCase());

            String id = null;

            //Percorrendo atrributos da classe
            BeanInfo beanInfo = Introspector.getBeanInfo(entityType);
            for (PropertyDescriptor propertyDesc : beanInfo.getPropertyDescriptors()) {
                String propertyName = propertyDesc.getName();
                Object value = propertyDesc.getReadMethod().invoke(entity);

                //obtendo ID do registro//
                if (propertyName != null && "_id".equals(propertyName) && value != null) {
                    id = value.toString();
                    break;
                }

            }

            if (id == null) {
                throw new IllegalArgumentException("Registro não possui um ID válido");
            }

            coll.deleteOne(new BasicDBObject("_id", new ObjectId(id)));
            //
        } catch (IntrospectionException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {

            StringBuilder message = new StringBuilder();
            message.append("Erro no banco de dados: ").append(e.getMessage());

            System.out.println(message.toString());
            return false;
        }

        return true;
    }

    @Override
    public <T extends GenericEntity> T getById(String id, Class<T> entityType) {

        if (id == null || id.isEmpty()) {
            return null;
        }

        try {

            //Obtendo objeto do MongoDatabase//
            MongoDatabase db = this.mongoClient.getDatabase(this.dbName);

            //Obtendo objeto da Table//
            MongoCollection<Document> coll = db.getCollection(entityType.getSimpleName().toLowerCase());

            FindIterable<Document> cursor = coll.find(new BasicDBObject("_id", new ObjectId(id)));

            Document result = cursor.first();

            if (result != null) {

                T resultObject = entityType.newInstance();

                resultObject.fullConstruct(result);
                return resultObject;
            }
            return null;
        } catch (InstantiationException | IllegalAccessException e) {

            StringBuilder message = new StringBuilder();
            message.append("Erro no o banco de dados: ").append(e.getMessage());

            System.out.println(message.toString());
            return null;
        }

    }

    @Override
    protected final Boolean connect() {

        try {

            if (this.servidores != null && this.servidores.isEmpty()) {
                throw new InvalidActivityException("Nenhum servidor de banco de dados foi informado.");
            }

            if (this.credenciais == null) {

                this.mongoClient = new MongoClient(this.servidores);
            } else {

                this.mongoClient = new MongoClient(this.servidores, this.credenciais);
            }

            return Boolean.TRUE;

        } catch (Exception e) {
            StringBuilder message = new StringBuilder();
            message.append("Erro na conexão com o banco de dados: ").append(e.getMessage());

            System.out.println(message.toString());

            return Boolean.FALSE;
        }

    }

    @Override
    protected Boolean disconnect() {
        throw new UnsupportedOperationException("Não é necessária implementação de um método disconect para o driver do mongoDB.");
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public Usuario loadUserByUsername(String username) {

        try {

            //Obtendo objeto do MongoDatabase//
            MongoDatabase db = this.mongoClient.getDatabase(this.dbName);

            //Obtendo objeto da Table//
            MongoCollection<Document> coll = db.getCollection("usuario");

            FindIterable<Document> cursor = coll.find(new BasicDBObject("username", username));

            Document result = cursor.first();

            if (result != null) {
                Usuario u = new Usuario();

                u.fullConstruct(result);
                return u;
            }

            return null;
        } catch (Exception e) {

            StringBuilder message = new StringBuilder();
            message.append("Erro no o banco de dados: ").append(e.getMessage());

            System.out.println(message.toString());
            return null;
        }

    }

    public <T extends GenericEntity> List<T> listar(Class<T> entityType) {
        try {

            //Obtendo objeto do MongoDatabase//
            MongoDatabase db = this.mongoClient.getDatabase(this.dbName);

            //Obtendo objeto da Table//
            MongoCollection<Document> coll = db.getCollection(entityType.getSimpleName().toLowerCase());

            FindIterable<Document> cursor = coll.find();

            //Criando lista dinâmica
            List<T> retorno = new ArrayList<>();

            //percorrendo resultado e populando a lista//
            cursor.forEach(new Block<Document>() {
                @Override
                public void apply(final Document document) {

                    try {
                        T resultObject = entityType.newInstance();
                        resultObject.fullConstruct(document);

                        retorno.add(resultObject);
                    } catch (InstantiationException | IllegalAccessException ex) {
                        Logger.getLogger(MongoDbDAO.class.getName()).log(Level.SEVERE, null, ex);
                        StringBuilder message = new StringBuilder();
                        message.append("Erro no o banco de dados: ").append(ex.getMessage());

                        System.out.println(message.toString());
                    }
                }
            });

            return retorno;
        } catch (Exception e) {

            StringBuilder message = new StringBuilder();
            message.append("Erro no o banco de dados: ").append(e.getMessage());

            System.out.println(message.toString());
            return null;
        }
    }

    public List<Session> listarAcessosAnalitico(Date dataAcesso) {

        try {

            //Obtendo objeto do MongoDatabase//
            MongoDatabase db = this.mongoClient.getDatabase(this.dbName);

            //Obtendo objeto da Table//
            MongoCollection<Document> coll = db.getCollection("session");

            FindIterable<Document> cursor;

            //filtro//
            if (dataAcesso != null) {
                String data = DateConverter.stringParse(dataAcesso);
                BasicDBObject filtro = new BasicDBObject("start", java.util.regex.Pattern.compile(data));
                cursor = coll.find(filtro);
            } else {
                cursor = coll.find();
            }

            List<Session> retorno = new ArrayList<>();

            //percorrendo resultado e populando a lista//
            cursor.forEach(new Block<Document>() {
                @Override
                public void apply(final Document document) {
                    Session s = new Session();
                    s.fullConstruct(document);
                    retorno.add(s);
                }
            });

            return retorno;
        } catch (Exception e) {

            StringBuilder message = new StringBuilder();
            message.append("Erro no o banco de dados: ").append(e.getMessage());

            System.out.println(message.toString());
            return null;
        }

    }

    public List<Cliente> listarAniversariantes(String dia, String mes) {
        try {

            //Obtendo objeto do MongoDatabase//
            MongoDatabase db = this.mongoClient.getDatabase(this.dbName);

            //Obtendo objeto da Table//
            MongoCollection<Document> coll = db.getCollection("cliente");

            FindIterable<Document> cursor;

            BasicDBObject filtro = new BasicDBObject();

            if (dia != null && !dia.isEmpty() && mes != null && !mes.isEmpty()) {
                String aux = dia + "/" + mes + "/";
                filtro.put("birthday", java.util.regex.Pattern.compile(aux));
            } else if (mes != null && !mes.isEmpty()) {
                String aux = "/" + mes + "/";
                filtro.put("birthday", java.util.regex.Pattern.compile(aux));
            } else if (dia != null && !dia.isEmpty()) {
                String aux = dia + "/";
                filtro.put("birthday", java.util.regex.Pattern.compile(aux));
            }

            //filtro//
            if (!filtro.isEmpty()) {
                cursor = coll.find(filtro);
            } else {
                cursor = coll.find();
            }

            List<Cliente> retorno = new ArrayList<>();

            //percorrendo resultado e populando a lista//
            cursor.forEach(new Block<Document>() {
                @Override
                public void apply(final Document document) {
                    Cliente c = new Cliente();
                    c.fullConstruct(document);
                    retorno.add(c);
                }
            });

            return retorno;
        } catch (Exception e) {

            StringBuilder message = new StringBuilder();
            message.append("Erro no o banco de dados: ").append(e.getMessage());

            System.out.println(message.toString());
            return null;
        }
    }

    public List<OsVO> listarPerfilDispositvosOS() {

        try {

            //Obtendo objeto do MongoDatabase//
            DB db = this.mongoClient.getDB(this.dbName);

            //Obtendo objeto da Table//
            DBCollection coll = db.getCollection("cliente");

            BasicDBObject query = new BasicDBObject();

            BasicDBObject fields = new BasicDBObject("device", true).append("_id", false);

            List<OsVO> retorno = new ArrayList<>();

            Long totalAndroid = 0L;
            Long totalWP = 0L;
            Long totalIOS = 0L;
            Long totalBB = 0L;
            Long totalw10 = 0L;

            DBCursor curs = coll.find(query, fields);
            while (curs.hasNext()) {
                DBObject o = curs.next();

                BasicDBList list = (BasicDBList) o.get("device");

                BasicDBObject[] resultArr = list.toArray(new BasicDBObject[0]);
                for (BasicDBObject dbObj : resultArr) {

                    if (dbObj.get("os") != null) {

                        switch (dbObj.get("os").toString()) {
                            case "ANDROID":
                                totalAndroid++;
                                break;
                            case "WINDOWS PHONE":
                                totalWP++;
                                break;
                            case "IOS":
                                totalIOS++;
                                break;
                            case "BLACKBERRY":
                                totalBB++;
                                break;
                            case "WINDOWS 10":
                                totalw10++;
                                break;
                        }

                    }

                }
            }

            retorno.add(new OsVO("ANDROID", totalWP));
            retorno.add(new OsVO("WINDOWS PHONE", totalAndroid));
            retorno.add(new OsVO("IOS", totalIOS));
            retorno.add(new OsVO("BLACKBERRY", totalBB));
            retorno.add(new OsVO("WINDOWS 10", totalw10));

            return retorno;
        } catch (Exception e) {

            StringBuilder message = new StringBuilder();
            message.append("Erro no o banco de dados: ").append(e.getMessage());

            System.out.println(message.toString());
            return null;
        }

    }

    public List<TipoVO> listarPerfilDispositvosTipo() {

        try {

            //Obtendo objeto do MongoDatabase//
            DB db = this.mongoClient.getDB(this.dbName);

            //Obtendo objeto da Table//
            DBCollection coll = db.getCollection("cliente");

            BasicDBObject query = new BasicDBObject();

            BasicDBObject fields = new BasicDBObject("device", true).append("_id", false);

            List<TipoVO> retorno = new ArrayList<>();

            Long totalMobile = 0L;
            Long totalNormal = 0L;

            DBCursor curs = coll.find(query, fields);
            while (curs.hasNext()) {
                DBObject o = curs.next();

                BasicDBList list = (BasicDBList) o.get("device");

                BasicDBObject[] resultArr = list.toArray(new BasicDBObject[0]);
                for (BasicDBObject dbObj : resultArr) {

                    if (dbObj.get("is_mobile") != null) {

                        switch (dbObj.get("is_mobile").toString()) {
                            case "true":
                                totalMobile++;
                                break;
                            case "false":
                                totalNormal++;
                                break;
                        }

                    }

                }
            }

            retorno.add(new TipoVO("MOBILE", totalMobile));
            retorno.add(new TipoVO("NORMAL", totalNormal));

            return retorno;
        } catch (Exception e) {

            StringBuilder message = new StringBuilder();
            message.append("Erro no o banco de dados: ").append(e.getMessage());

            System.out.println(message.toString());
            return null;
        }
    }

    public List<BrowserVO> listarPerfilDispositvosBrowser() {

        try {

            //Obtendo objeto do MongoDatabase//
            DB db = this.mongoClient.getDB(this.dbName);

            //Obtendo objeto da Table//
            DBCollection coll = db.getCollection("cliente");

            BasicDBObject query = new BasicDBObject();

            BasicDBObject fields = new BasicDBObject("device", true).append("_id", false);

            List<BrowserVO> retorno = new ArrayList<>();

            Long totalFirefox = 0L;
            Long totalChrome = 0L;
            Long totalEdge = 0L;
            Long totalIE = 0L;
            Long totalOpera = 0L;
            Long totalSafari = 0L;

            DBCursor curs = coll.find(query, fields);
            while (curs.hasNext()) {
                DBObject o = curs.next();

                BasicDBList list = (BasicDBList) o.get("device");

                BasicDBObject[] resultArr = list.toArray(new BasicDBObject[0]);
                for (BasicDBObject dbObj : resultArr) {

                    if (dbObj.get("browser") != null) {

                        switch (dbObj.get("browser").toString()) {
                            case "FIREFOX":
                                totalFirefox++;
                                break;
                            case "SAFARI":
                                totalSafari++;
                                break;
                            case "CHROME":
                                totalChrome++;
                                break;
                            case "EDGE":
                                totalEdge++;
                                break;
                            case "OPERA":
                                totalOpera++;
                                break;
                            case "IE":
                                totalIE++;
                                break;
                        }

                    }

                }
            }

            retorno.add(new BrowserVO("FIREFOX", totalFirefox));
            retorno.add(new BrowserVO("CHROME", totalChrome));
            retorno.add(new BrowserVO("EDGE", totalEdge));
            retorno.add(new BrowserVO("OPERA", totalOpera));
            retorno.add(new BrowserVO("SAFARI", totalSafari));
            retorno.add(new BrowserVO("IE", totalIE));

            return retorno;
        } catch (Exception e) {

            StringBuilder message = new StringBuilder();
            message.append("Erro no o banco de dados: ").append(e.getMessage());

            System.out.println(message.toString());
            return null;
        }
    }

    public List<ResolutionVO> listarPerfilDispositvosResolution() {

        try {

            //Obtendo objeto do MongoDatabase//
            DB db = this.mongoClient.getDB(this.dbName);

            //Obtendo objeto da Table//
            DBCollection coll = db.getCollection("cliente");

            BasicDBObject query = new BasicDBObject();

            BasicDBObject fields = new BasicDBObject("device", true).append("_id", false);

            List<ResolutionVO> retorno = new ArrayList<>();

            Long total1 = 0L;
            Long total2 = 0L;
            Long total3 = 0L;
            Long total4 = 0L;
            Long total5 = 0L;

            DBCursor curs = coll.find(query, fields);
            while (curs.hasNext()) {
                DBObject o = curs.next();

                BasicDBList list = (BasicDBList) o.get("device");

                BasicDBObject[] resultArr = list.toArray(new BasicDBObject[0]);
                for (BasicDBObject dbObj : resultArr) {

                    if (dbObj.get("resolution") != null) {

                        switch (dbObj.get("resolution").toString()) {
                            case "240×320":
                                total1++;
                                break;
                            case "128X160":
                                total2++;
                                break;
                            case "1600x1200":
                                total3++;
                                break;
                            case "720X1080":
                                total4++;
                                break;
                            case "1280x1024":
                                total5++;
                                break;
                        }

                    }

                }
            }

            retorno.add(new ResolutionVO("240×320", total1));
            retorno.add(new ResolutionVO("128X160", total2));
            retorno.add(new ResolutionVO("1600x1200", total3));
            retorno.add(new ResolutionVO("720X1080", total4));
            retorno.add(new ResolutionVO("1280x1024", total5));

            return retorno;
        } catch (Exception e) {

            StringBuilder message = new StringBuilder();
            message.append("Erro no o banco de dados: ").append(e.getMessage());

            System.out.println(message.toString());
            return null;
        }
    }

    public List<TipoLoginVO> listarPerfilClientesTipoLogin() {
        try {

            //Obtendo objeto do MongoDatabase//
            DB db = this.mongoClient.getDB(this.dbName);

            //Obtendo objeto da Table//
            DBCollection coll = db.getCollection("cliente");

            BasicDBObject query = new BasicDBObject();

            BasicDBObject fields = new BasicDBObject("typeLogin", true).append("_id", false);

            List<TipoLoginVO> retorno = new ArrayList<>();

            Long totalN = 0L;
            Long totalF = 0L;

            DBCursor curs = coll.find(query, fields);
            while (curs.hasNext()) {
                DBObject o = curs.next();

                BasicDBObject dbObj = (BasicDBObject) o;

                if (dbObj.get("typeLogin") != null) {

                    switch (dbObj.get("typeLogin").toString()) {
                        case "normal":
                            totalN++;
                            break;
                        case "facebook":
                            totalF++;
                            break;
                    }

                }
            }

            retorno.add(new TipoLoginVO("Normal", totalN));
            retorno.add(new TipoLoginVO("Facebook", totalF));

            return retorno;
        } catch (Exception e) {

            StringBuilder message = new StringBuilder();
            message.append("Erro no o banco de dados: ").append(e.getMessage());

            System.out.println(message.toString());
            return null;
        }
    }

    public List<AmigoVO> listarPerfilClientesAmigos() {
        try {

            //Obtendo objeto do MongoDatabase//
            DB db = this.mongoClient.getDB(this.dbName);

            //Obtendo objeto da Table//
            DBCollection coll = db.getCollection("cliente");

            BasicDBObject query = new BasicDBObject();

            BasicDBObject fields = new BasicDBObject("clienteFacebook", true).append("_id", false);

            List<AmigoVO> retorno = new ArrayList<>();

            Map<Integer, Long> totalMap = new HashMap<>();

            DBCursor curs = coll.find(query, fields);
            while (curs.hasNext()) {
                DBObject o = curs.next();

                BasicDBObject dbObj = (BasicDBObject) o.get("clienteFacebook");

                if (dbObj != null && dbObj.get("friendList") != null) {

                    BasicDBList list = (BasicDBList) dbObj.get("friendList");

                    BasicDBObject[] resultArr = list.toArray(new BasicDBObject[0]);

                    Integer qtdAmigos = resultArr.length;

                    //Acumulando dados no Mapa//
                    if (totalMap.containsKey(qtdAmigos)) {

                        Long val = totalMap.get(qtdAmigos);
                        val++;
                        totalMap.replace(qtdAmigos, val);

                    } else {

                        totalMap.put(qtdAmigos, 1L);
                    }

                }
            }

            //Convertendo p/ lista//
            totalMap.entrySet().stream().forEach((es) -> {
                retorno.add(new AmigoVO(es.getKey().toString(), es.getValue()));
            });

            return retorno;
        } catch (Exception e) {

            StringBuilder message = new StringBuilder();
            message.append("Erro no o banco de dados: ").append(e.getMessage());

            System.out.println(message.toString());
            return null;
        }
    }

    public List<GeneroVO> listarPerfilClientesGenero() {
        try {

            //Obtendo objeto do MongoDatabase//
            DB db = this.mongoClient.getDB(this.dbName);

            //Obtendo objeto da Table//
            DBCollection coll = db.getCollection("cliente");

            BasicDBObject query = new BasicDBObject();

            BasicDBObject fields = new BasicDBObject("gender", true).append("_id", false);

            List<GeneroVO> retorno = new ArrayList<>();

            Long totalM = 0L;
            Long totalF = 0L;

            DBCursor curs = coll.find(query, fields);
            while (curs.hasNext()) {
                DBObject o = curs.next();

                BasicDBObject dbObj = (BasicDBObject) o;

                if (dbObj.get("gender") != null) {

                    switch (dbObj.get("gender").toString()) {
                        case "Male":
                            totalM++;
                            break;
                        case "Female":
                            totalF++;
                            break;
                    }

                }
            }

            retorno.add(new GeneroVO("Masculino", totalM));
            retorno.add(new GeneroVO("Feminino", totalF));

            return retorno;
        } catch (Exception e) {

            StringBuilder message = new StringBuilder();
            message.append("Erro no o banco de dados: ").append(e.getMessage());

            System.out.println(message.toString());
            return null;
        }
    }

    public List<FaixaEtariaVO> listarPerfilClientesFaixaEtaria() {
        try {

            //Obtendo objeto do MongoDatabase//
            DB db = this.mongoClient.getDB(this.dbName);

            //Obtendo objeto da Table//
            DBCollection coll = db.getCollection("cliente");

            BasicDBObject query = new BasicDBObject();

            BasicDBObject fields = new BasicDBObject("birthday", true).append("_id", false);

            List<FaixaEtariaVO> retorno = new ArrayList<>();

            Map<Integer, Long> totalMap = new HashMap<>();

            DBCursor curs = coll.find(query, fields);
            while (curs.hasNext()) {
                DBObject o = curs.next();

                BasicDBObject dbObj = (BasicDBObject) o;

                if (dbObj.get("birthday") != null) {
                    Date dataNasc = DateConverter.dateParse(dbObj.get("birthday").toString());

                    //Calculando idade//
                    Calendar dataNascCal = Calendar.getInstance();
                    dataNascCal.setTime(dataNasc);
                    Calendar hoje = Calendar.getInstance();
                    int idade = hoje.get(Calendar.YEAR) - dataNascCal.get(Calendar.YEAR);
                    if (hoje.get(Calendar.MONTH) < dataNascCal.get(Calendar.MONTH)) {
                        idade--;
                    } else if (hoje.get(Calendar.MONTH) == dataNascCal.get(Calendar.MONTH)
                            && hoje.get(Calendar.DAY_OF_MONTH) < dataNascCal.get(Calendar.DAY_OF_MONTH)) {
                        idade--;
                    }

                    //Acumulando dados no Mapa//
                    if (totalMap.containsKey(idade)) {

                        Long val = totalMap.get(idade);
                        val++;
                        totalMap.replace(idade, val);

                    } else {

                        totalMap.put(idade, 1L);
                    }

                }
            }

            //Convertendo p/ lista//
            totalMap.entrySet().stream().forEach((es) -> {
                retorno.add(new FaixaEtariaVO(es.getKey().toString(), es.getValue()));
            });

            return retorno;
        } catch (Exception e) {

            StringBuilder message = new StringBuilder();
            message.append("Erro no o banco de dados: ").append(e.getMessage());

            System.out.println(message.toString());
            return null;
        }
    }

}
