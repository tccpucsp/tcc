package com.tcc.dao;

import com.tcc.entity.Agregado;
import com.tcc.entity.GenericEntity;

public abstract class GenericDAO {

    protected GenericDAO() {
    }

    protected abstract <T extends GenericEntity, E extends Agregado> Boolean insert(Object entity, Class<T> entityType, Class<E> agregadoType);

    protected abstract <T extends GenericEntity, E extends Agregado> Boolean update(Object entity, Class<T> entityType, Class<E> agregadoType);

    protected abstract <T extends GenericEntity> Boolean delete(Object entity, Class<T> entityType);

    protected abstract <T extends GenericEntity> T getById(String id, Class<T> entityType);

    protected abstract Boolean connect();

    protected abstract Boolean disconnect();

}
