package com.tcc.task;

import com.tcc.entity.Processo;
import com.tcc.service.ProcessoService;
import java.util.Observable;
import java.util.Observer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class ProcessoController implements Observer {

    @Autowired
    private ProcessoService service;

    @Override
    public void update(Observable pManager, Object pProcess) {

        try {

            if (pProcess instanceof AbstractProcess) {
                AbstractProcess processo = (AbstractProcess) pProcess;
                service.inserir(processo.getProcess());
            } else if (pProcess instanceof Processo) {
                Processo processo = (Processo) pProcess;
                service.persistir(processo);
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
