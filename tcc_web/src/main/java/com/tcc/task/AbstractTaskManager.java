package com.tcc.task;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;

public abstract class AbstractTaskManager extends Observable {

    @Autowired
    private ThreadPoolTaskScheduler taskScheduler;
    private ScheduledThreadPoolExecutor schedulerExecutor;
    private Map<String, ScheduledFuture> futures;
    private Map<String, AbstractProcess> processMap;

    public void schedule(AbstractProcess process, Trigger trigger) {
        if (!getProcessMap().containsKey(process.getId())) {
            addProcess(process);
            notifySchedule(process);
            if (getProcessMap().get(process.getId()).getAtivo()) {
                ScheduledFuture future = taskScheduler.schedule(process, trigger);
                getFutures().put(process.getId(), future);
            }
        } else {
            ScheduledFuture future = taskScheduler.schedule(process, trigger);
            getFutures().put(process.getId(), future);
        }
    }

    public ScheduledThreadPoolExecutor getSchedulerExecutor() {
        if (schedulerExecutor == null) {
            schedulerExecutor = (ScheduledThreadPoolExecutor) this.taskScheduler.getScheduledExecutor();
        }
        return schedulerExecutor;
    }

    public Map<String, ScheduledFuture> getFutures() {
        if (futures == null) {
            futures = new HashMap<>();
        }
        return futures;
    }

    public Map<String, AbstractProcess> getProcessMap() {
        if (processMap == null) {
            processMap = new HashMap<>();
        }
        return processMap;
    }

    private void addProcess(AbstractProcess process) {
        getProcessMap().put(process.getId(), process);
        process.getProcess().setAtivo(true);
    }

    public Boolean cancel(String processoId) {
        Boolean isCanceled = false;
        if (futures.containsKey(processoId)) {
            isCanceled = futures.get(processoId).cancel(false);
        }
        if (isCanceled) {
            processMap.get(processoId).setAtivo(false);
            notifyProcess(processMap.get(processoId));
        }
        return isCanceled;
    }

    public Boolean cancelAndStop(String processoId) {
        Boolean isCanceled = false;
        if (futures.containsKey(processoId)) {
            isCanceled = futures.get(processoId).cancel(true);
        }
        if (isCanceled) {
            processMap.get(processoId).setAtivo(false);
            notifyProcess(processMap.get(processoId));
        }
        return isCanceled;
    }

    public Boolean remove(String processoId) {
        Boolean isRemoved = false;
        if (futures.containsKey(processoId)) {
            isRemoved = getSchedulerExecutor().remove(processMap.get(processoId));
        }
        if (isRemoved) {
            processMap.get(processoId).setAtivo(false);
            notifyProcess(processMap.get(processoId));
        }
        return isRemoved;
    }

    public Boolean executar(String processoId) {
        Boolean isExec = false;
        if (processMap.containsKey(processoId)) {
            getSchedulerExecutor().execute(processMap.get(processoId));
            isExec = true;
        }
        return isExec;
    }

    public void agendar(AbstractProcess process) {
        this.schedule(process, new CronTrigger(process.getPeriodicidade()));
    }

    public void notifyProcess(AbstractProcess process) {
        setChanged();
        notifyObservers(process.getProcess());
    }

    public void notifySchedule(AbstractProcess process) {
        setChanged();
        notifyObservers(process);
    }
}
