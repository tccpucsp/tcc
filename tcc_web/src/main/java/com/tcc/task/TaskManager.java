package com.tcc.task;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

@Component
public class TaskManager extends AbstractTaskManager {

    @Autowired
    private FacebookGraphTask fbTask;
    @Autowired
    private ProcessoController processoController;
    @Autowired
    private ProcessoExecucaoController processoExecucaoController;
    @Autowired
    private ProcessoExecucaoLogController processoExecucaoLogController;

    @PostConstruct
    public void inicializar() {
        addObserver(processoController);
        agendar();
        processoExecucaoController.registrarProcessos(getProcessMap().values());
        processoExecucaoLogController.registrarProcessos(getProcessMap().values());
    }

    public final void agendar() {
        this.schedule(fbTask, new CronTrigger(fbTask.getPeriodicidade()));
    }
}
