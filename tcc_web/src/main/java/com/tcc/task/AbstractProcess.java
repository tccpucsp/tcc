package com.tcc.task;

import com.tcc.entity.Processo;
import com.tcc.entity.ProcessoExecucao;
import com.tcc.entity.ProcessoExecucaoLog;
import com.tcc.service.ProcessoExecucaoService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Observable;

public abstract class AbstractProcess extends Observable implements Runnable {

    private Processo processo;
    private ProcessoExecucao execucao;

    public AbstractProcess(String id) {
        this(id, null);
    }

    public AbstractProcess(String id, String descricao) {
        criarProcesso(id, descricao);
    }

    public final void criarProcesso(String id, String descricao) {
        if (descricao == null) {
            descricao = getClass().getSimpleName();
        }
        processo = new Processo(id, id, descricao, null, false, null);
    }

    protected abstract void executar(Log log);

    @Override
    public void run() {
        iniciarExecucao();
        Log log = new Log();
        log.addLog("Iniciando execução...");
        executar(log);
        log.addLog("Termino da execução...");
        finalizarExecucao();
    }

    public void iniciarExecucao() {
        execucao = new ProcessoExecucao();
        execucao.setSituacao(ProcessoExecucaoService.EM_EXECUCAO);
        execucao.setDataInicio(new Date());
        execucao.setProcesso(getProcess());
        notificarExecucao(execucao);
    }

    public void finalizarExecucao() {
        execucao.setSituacao(ProcessoExecucaoService.PARADO);
        execucao.setDataFinal(new Date());
        notificarExecucao(execucao);
    }

    public ProcessoExecucao getExecucao() {
        return execucao;
    }

    public Processo getProcess() {
        return processo;
    }

    protected void notificarLog(ProcessoExecucaoLog log) {
        setChanged();
        notifyObservers(log);
    }

    private void notificarExecucao(ProcessoExecucao execucao) {
        setChanged();
        notifyObservers(execucao);
    }

    public String getId() {
        return this.processo.get_id();
    }

    public String getDescricao() {
        if (this.processo.getDescricao() == null) {
            this.processo.setDescricao(getClass().getSimpleName());
        }
        return this.processo.getDescricao();
    }

    public void setDescricao(String descricao) {
        this.processo.setDescricao(descricao);
    }

    public Date getDataInicio() {
        return this.processo.getDataInicio();
    }

    public void setDataInicio(Date dataInicio) {
        this.processo.setDataInicio(dataInicio);
    }

    public String getPeriodicidade() {
        return this.processo.getPeriodicidade();
    }

    public void setPeriodicidade(String periodicidade) {
        this.processo.setPeriodicidade(periodicidade);
    }

    public Boolean getAtivo() {
        return this.processo.getAtivo();
    }

    public void setAtivo(Boolean ativo) {
        this.processo.setAtivo(ativo);
    }

    public class Log {

        private List<ProcessoExecucaoLog> logs;

        public Log() {
            this.logs = new ArrayList<>();
        }

        public void addLog(String logDescricao) {
            ProcessoExecucaoLog log = new ProcessoExecucaoLog(null, execucao, logDescricao, new Date());
            this.logs.add(log);
            notificarLog(log);
        }
    }
}
