package com.tcc.task;

import com.tcc.service.FacebookGraphService;
import org.springframework.beans.factory.annotation.Autowired;

public class FacebookGraphTask extends AbstractProcess {

    public FacebookGraphTask() {
        super("1");
        //Rodando todo dia as 2 horas da manhã//
//        setPeriodicidade("* * 2 * * *");
        //a cada 30 segundos//
//        setPeriodicidade("0/30 * * * * *");
        //a cada 1 hora//
        setPeriodicidade("* 0/60 * * * *");
    }
    @Autowired
    private FacebookGraphService fbService;

    @Override
    protected void executar(Log log) {

        fbService.processarClientes();
    }
}
