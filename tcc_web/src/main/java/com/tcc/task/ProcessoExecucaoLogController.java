package com.tcc.task;

import com.tcc.entity.ProcessoExecucaoLog;
import com.tcc.service.ProcessoExecucaoLogService;
import java.util.Collection;
import java.util.Observable;
import java.util.Observer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class ProcessoExecucaoLogController implements Observer {

    @Autowired
    private ProcessoExecucaoLogService service;
    private AbstractProcess processo;

    @Override
    public void update(Observable o, Object pLog) {
        if (pLog == null || !(pLog instanceof ProcessoExecucaoLog)) {
            return;
        }

        processo = (AbstractProcess) o;

        try {
            service.persistir((ProcessoExecucaoLog) pLog);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void registrarProcessos(Collection<AbstractProcess> collection) {
        for (AbstractProcess p : collection) {
            p.addObserver(this);
        }
    }
}
