package com.tcc.task;

import com.tcc.entity.ProcessoExecucao;
import com.tcc.service.ProcessoExecucaoService;
import java.util.Collection;
import java.util.Observable;
import java.util.Observer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class ProcessoExecucaoController implements Observer {

    @Autowired
    private ProcessoExecucaoService service;
    private AbstractProcess processo;

    @Override
    public void update(Observable pProcesso, Object pExecucao) {
        if (pExecucao == null || !(pExecucao instanceof ProcessoExecucao)) {
            return;
        }

        processo = (AbstractProcess) pProcesso;

        try {
            service.persistir((ProcessoExecucao) pExecucao);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void registrarProcessos(Collection<AbstractProcess> collection) {
        for (AbstractProcess p : collection) {
            p.addObserver(this);
        }
    }
}
