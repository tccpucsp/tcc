package com.tcc.config;

import com.tcc.cript.base.Cripter;
import com.tcc.cript.impl.MD5Cripter;
import com.tcc.utils.converters.BigDecimalConverter;
import com.tcc.utils.converters.CalendarConverter;
import com.tcc.utils.converters.DateConverter;
import com.tcc.utils.converters.StringConverter;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebMvcContextConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(stringConverter());
        registry.addConverter(dateConverter());
        registry.addConverter(calendarConverter());
        registry.addConverter(bigDecimalConverter());
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/META-INF/web-resources/").setCachePeriod(31556926);
    }

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.favorPathExtension(false).favorParameter(true);
    }

    @Override
    public void configureDefaultServletHandling(final DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Bean
    public Cripter cripter() {

        return new MD5Cripter();
    }

    @Bean
    public StringConverter stringConverter() {

        return new StringConverter();
    }

    @Bean
    public CalendarConverter calendarConverter() {

        return new CalendarConverter();
    }

    @Bean
    public DateConverter dateConverter() {

        return new DateConverter();
    }

    @Bean
    public BigDecimalConverter bigDecimalConverter() {

        return new BigDecimalConverter();
    }
}
