package com.tcc.cript.impl;

import com.tcc.cript.base.Cripter;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

@Component
public class MD5Cripter implements Cripter {

    @Override
    public String get(String senha) throws Exception {
        return DigestUtils.md5Hex(senha);
    }
}
