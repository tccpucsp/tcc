package com.tcc.cript.base;

//Aqui sera a interface referente a criptografia de senha.
public interface Cripter {

    public String get(String senha) throws Exception;
}