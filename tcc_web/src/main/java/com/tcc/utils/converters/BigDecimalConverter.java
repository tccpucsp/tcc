package com.tcc.utils.converters;

import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.core.convert.converter.Converter;

public class BigDecimalConverter implements Converter<String, BigDecimal> {

    @Override
    public BigDecimal convert(String arg0) {
        BigDecimal bigDecimal = null;

        if (arg0 == null) {
            return null;
        } else if (arg0.length() <= 0) {
            return null;
        } else if (arg0.length() >= 1) {
            try {
                double valor = Double.parseDouble(arg0.replaceAll("\\.", "").replaceAll(",", "."));
                bigDecimal = new BigDecimal(valor);
            } catch (Exception ex) {
                Logger.getLogger(BigDecimalConverter.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }

        return bigDecimal;
    }
}
