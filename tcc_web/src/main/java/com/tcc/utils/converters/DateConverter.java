package com.tcc.utils.converters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.core.convert.converter.Converter;

public class DateConverter implements Converter<String, Date> {

    @Override
    public Date convert(String arg0) {
        SimpleDateFormat sdf;
        if (arg0 == null) {
            return null;
        } else if (arg0.length() == 19) {
            sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        } else if (arg0.length() == 16) {
            sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        } else if (arg0.length() == 10) {
            sdf = new SimpleDateFormat("dd/MM/yyyy");
        } else {
            return null;
        }

        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(arg0));
        } catch (ParseException e) {
            return null;
        }
        return c.getTime();
    }

    public static Date dateParse(String date) {
        SimpleDateFormat sdf;
        if (date == null) {
            return null;
        } else if (date.length() > 19) {

            date = date.replace("BRST ", "");
            sdf = new SimpleDateFormat("EE MMM dd HH:mm:ss yyyy", Locale.ENGLISH);
        } else if (date.length() == 19) {
            sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        } else if (date.length() == 16) {
            sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        } else if (date.length() == 10) {
            sdf = new SimpleDateFormat("dd/MM/yyyy");
        } else {
            return null;
        }

        try {
            return sdf.parse(date);
        } catch (ParseException ex) {
            Logger.getLogger(DateConverter.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static String stringParse(Date date) {

        if (date == null) {
            return null;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        return sdf.format(date);
    }
}
