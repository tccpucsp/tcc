package com.tcc.utils.converters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.springframework.core.convert.converter.Converter;

public class CalendarConverter implements Converter<String, Calendar> {

    @Override
    public Calendar convert(String arg0) {
        SimpleDateFormat sdf;
        if (arg0 == null) {
            return null;
        } else if (arg0.length() == 19) {
            sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        } else if (arg0.length() == 16) {
            sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        } else if (arg0.length() == 10) {
            sdf = new SimpleDateFormat("dd/MM/yyyy");
        } else {
            return null;
        }

        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(arg0));
        } catch (ParseException e) {
            return null;
        }
        return c;
    }
}
