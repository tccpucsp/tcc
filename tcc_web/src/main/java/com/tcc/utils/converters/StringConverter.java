package com.tcc.utils.converters;

import org.springframework.core.convert.converter.Converter;

public class StringConverter implements Converter<String, String> {

    @Override
    public String convert(String arg0) {

        if (arg0 == null) {
            return "";
        } else {
            return (arg0);
        }
    }
}
