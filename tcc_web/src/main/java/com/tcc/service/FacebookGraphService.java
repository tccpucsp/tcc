package com.tcc.service;

import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Version;
import com.restfb.types.User;
import com.tcc.dao.MongoDbDAO;
import com.tcc.entity.Cliente;
import com.tcc.entity.ClienteFacebook;
import com.tcc.entity.ClienteFacebookAmigo;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("facebookGraphService")
public class FacebookGraphService {

    @Autowired
    private MongoDbDAO dao;

    public FacebookGraphService() {
    }

    public void processarClientes() {

        List<Cliente> clientes = dao.listar(Cliente.class);

        if (clientes != null && !clientes.isEmpty()) {
            clientes.stream().forEach((cliente) -> {
                if (cliente.getClienteFacebook() != null) {
                    fetchFacebookInfo(cliente);
                }
            });
        }

    }

    private void fetchFacebookInfo(Cliente c) {
        //Declarando GRAPH API
        FacebookClient facebookClient = new DefaultFacebookClient(c.getClienteFacebook().getToken(), Version.LATEST);

        //Buscando Amigos//
        Connection<User> friendList = facebookClient.fetchConnection("me/friends", User.class);

        List<ClienteFacebookAmigo> amigos = new ArrayList<>();
        // Percorrendo amigos
        for (List<User> friendConnections : friendList) {
            friendConnections.stream().forEach((friend) -> {
                amigos.add(new ClienteFacebookAmigo(friend.getId(), friend.getName()));
            });
        }

        User user = facebookClient.fetchObject("me", User.class);

        //Gravando amigos//
        persistirCliente(amigos, c, user.getName(), user.getId());

    }

    private void persistirCliente(List<ClienteFacebookAmigo> amigos, Cliente c, String name, String id) {

        c.getClienteFacebook().setId(id);
        c.getClienteFacebook().setName(name);

        c.getClienteFacebook().setFriendList(amigos);

        dao.update(c, Cliente.class, ClienteFacebook.class);

    }

}
