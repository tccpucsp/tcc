package com.tcc.service;

import com.tcc.dao.MongoDbDAO;
import com.tcc.entity.ProcessoExecucaoLog;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("processoExecucaoLogService")
public class ProcessoExecucaoLogService {

    @Autowired
    private MongoDbDAO dao;

    public ProcessoExecucaoLog obter(String id) throws Exception {
        return dao.getById(id, ProcessoExecucaoLog.class);

    }

    public ProcessoExecucaoLog persistir(ProcessoExecucaoLog processoExecucaoLog) throws Exception {

        validar(processoExecucaoLog);

        try {
            if (processoExecucaoLog.get_id() != null) {
                dao.update(processoExecucaoLog, ProcessoExecucaoLog.class, null);
            } else {
                dao.insert(processoExecucaoLog, ProcessoExecucaoLog.class, null);
            }

            return processoExecucaoLog;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    private void validar(ProcessoExecucaoLog processoExecucaoLog) throws Exception {

        if (processoExecucaoLog == null) {
            throw new Exception("Erro ao validar registro: Registro nulo. ");
        }

    }

    public void remover(String id) throws Exception {
        dao.delete(dao.getById(id, ProcessoExecucaoLog.class), ProcessoExecucaoLog.class);
    }

    public List<ProcessoExecucaoLog> listar() {
        return dao.listar(ProcessoExecucaoLog.class);
    }
}
