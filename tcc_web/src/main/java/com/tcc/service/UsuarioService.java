package com.tcc.service;

import com.tcc.dao.MongoDbDAO;
import com.tcc.entity.Usuario;
import com.tcc.exception.TccException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("usuarioService")
public class UsuarioService {

    @Autowired
    private MongoDbDAO dao;

    public Usuario getById(String usuarioLogadoId) {

        if (usuarioLogadoId == null || usuarioLogadoId.trim().isEmpty()) {
            return null;
        }

        return dao.getById(usuarioLogadoId, Usuario.class);
    }

    public Usuario loadUserByUsername(String username) throws TccException {

        if (username == null || username.isEmpty()) {

            TccException tccE = new TccException();
            tccE.addError(null, "Usuário inválido!");

            throw tccE;
        }

        return dao.loadUserByUsername(username);

    }

    public List<Usuario> listar() {
        return dao.listar(Usuario.class);
    }

    public void persistir(Usuario u) throws TccException {

        validar(u);

        //novo registro//
        if (u.get_id() == null || u.get_id().trim().isEmpty()) {

            dao.insert(u, Usuario.class, null);

        } else {

            dao.update(u, Usuario.class, null);
        }

    }

    private void validar(Usuario u) throws TccException {
        TccException te = new TccException();

        if (u == null) {
        } else {

            if (u.getUsername() == null || u.getUsername().isEmpty()) {
                te.addError(null, "Login não foi informado!");
            }

            if (u.getPassword() == null || u.getPassword().isEmpty()) {
                te.addError(null, "Senha não foi informada!");
            }

            if (u.getEmail() == null || u.getEmail().isEmpty()) {
                te.addError(null, "E-mail não foi informado!");
            }

            if (u.getName() == null || u.getName().isEmpty()) {
                te.addError(null, "Nome não foi informado!");
            }
        }

        if (!te.isEmpty()) {
            throw te;
        }
    }

    public void remover(String id) {
        if (id != null && !id.trim().isEmpty()) {
            dao.delete(new Usuario(id), Usuario.class);
        }
    }

    public Usuario getUserNewPass(String login) throws TccException {

        if (login == null || login.isEmpty()) {
            TccException te = new TccException();
            te.addError(null, "Usuário não foi informado!");
            throw te;
        }

        Usuario u = dao.loadUserByUsername(login);

        Double newPass = Math.random() * Calendar.getInstance().getTimeInMillis();
        String newPassStr = newPass.toString().substring(0, 9).replace(".", "");

        u.setPassword(newPassStr);

        return u;

    }

}
