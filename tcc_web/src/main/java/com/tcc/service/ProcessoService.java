package com.tcc.service;

import com.tcc.dao.MongoDbDAO;
import com.tcc.entity.Processo;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("processoService")
public class ProcessoService {

    @Autowired
    private MongoDbDAO dao;

    public Processo obter(String id) throws Exception {
        return dao.getById(id, Processo.class);
    }

    public Processo persistir(Processo processo) throws Exception {

        validar(processo);

        try {

            //persistindo//
            if (processo.get_id() != null) {
                processo.setDataInicio(Calendar.getInstance().getTime());
                dao.update(processo, Processo.class, null);
            } else {
                dao.insert(processo, Processo.class, null);
            }

            return processo;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public Processo inserir(Processo processo) throws Exception {

        validar(processo);

        Processo p = dao.getById(processo.get_id(), Processo.class);

        try {
            if (p == null) {
                processo.setDataInicio(Calendar.getInstance().getTime());
                dao.insert(processo, Processo.class, null);
            } else {
                processo.setAtivo(p.getAtivo());
                processo.setCodigo(p.getCodigo());
                processo.setDataInicio(p.getDataInicio());
                processo.setDescricao(p.getDescricao());
                processo.setPeriodicidade(p.getPeriodicidade());
            }

            return processo;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    private void validar(Processo processo) throws Exception {

        if (processo == null) {

            throw new Exception("Erro ao validar registro: Registro nulo. ");
        } else {

            if (processo.getCodigo() == null) {

                throw new Exception("Erro ao validar registro: Campo codigo nulo.");
            } else if (processo.getCodigo().trim().isEmpty()) {

                throw new Exception("Erro ao validar registro: Campo codigo não informado corretamente.");
            }
        }
    }

    public void remover(String id) throws Exception {
        dao.delete(dao.getById(id, Processo.class), Processo.class);
    }

    public List<Processo> listar() {
        return dao.listar(Processo.class);
    }
}
