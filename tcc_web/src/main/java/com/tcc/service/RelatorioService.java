package com.tcc.service;

import com.tcc.dao.MongoDbDAO;
import com.tcc.entity.Cliente;
import com.tcc.entity.Session;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vo.AmigoVO;
import vo.BrowserVO;
import vo.FaixaEtariaVO;
import vo.GeneroVO;
import vo.OsVO;
import vo.ResolutionVO;
import vo.TipoLoginVO;
import vo.TipoVO;

@Service("relatorioService")
public class RelatorioService {

    @Autowired
    private MongoDbDAO dao;

    public RelatorioService() {
    }

    public List<Session> listarAcessosAnalitico(Date dataAcesso) {

        return dao.listarAcessosAnalitico(dataAcesso);

    }

    public List<Cliente> listarAniversariantes(String dia, String mes) {

        return dao.listarAniversariantes(dia, mes);
    }

    public List<OsVO> listarPerfilDispositvosOS() {
        return dao.listarPerfilDispositvosOS();
    }

    public List<TipoVO> listarPerfilDispositvosTipo() {
        return dao.listarPerfilDispositvosTipo();
    }

    public List<BrowserVO> listarPerfilDispositvosBrowser() {
        return dao.listarPerfilDispositvosBrowser();
    }

    public List<ResolutionVO> listarPerfilDispositvosResolution() {
        return dao.listarPerfilDispositvosResolution();
    }

    public List<TipoLoginVO> listarPerfilClientesTipoLogin() {
        return dao.listarPerfilClientesTipoLogin();
    }

    public List<AmigoVO> listarPerfilClientesAmigos() {
        return dao.listarPerfilClientesAmigos();
    }

    public List<GeneroVO> listarPerfilClientesGenero() {
        return dao.listarPerfilClientesGenero();
    }

    public List<FaixaEtariaVO> listarPerfilClientesFaixaEtaria() {
        return dao.listarPerfilClientesFaixaEtaria();
    }

}
