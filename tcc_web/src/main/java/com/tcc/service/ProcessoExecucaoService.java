package com.tcc.service;

import com.tcc.dao.MongoDbDAO;
import com.tcc.entity.ProcessoExecucao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("processoExecucaoService")
public class ProcessoExecucaoService {

    public final static String EM_EXECUCAO = "em_execucao";
    public final static String PARADO = "parado";

    @Autowired
    private MongoDbDAO dao;

    public ProcessoExecucao obter(String id) throws Exception {
        return dao.getById(id, ProcessoExecucao.class);
    }

    public ProcessoExecucao persistir(ProcessoExecucao processoExecucao) throws Exception {

        validar(processoExecucao);

        try {
            if (processoExecucao.get_id() != null) {
                dao.update(processoExecucao, ProcessoExecucao.class, null);
            } else {
                dao.insert(processoExecucao, ProcessoExecucao.class, null);
            }

            return processoExecucao;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public void validar(ProcessoExecucao processoExecucao) throws Exception {

        if (processoExecucao == null) {

            throw new Exception("Erro ao validar registro: Registro nulo. ");
        } else {

            if (processoExecucao.getProcesso() == null) {

                throw new Exception("Erro ao validar registro: Campo processo nulo.");
            }
            if (processoExecucao.getSituacao() == null) {

                throw new Exception("Erro ao validar registro: Campo situacao nulo.");
            } else if (processoExecucao.getSituacao().trim().isEmpty()) {

                throw new Exception("Erro ao validar registro: Campo situacao não informado corretamente.");
            }
        }
    }

    public void remover(String id) throws Exception {
        dao.delete(dao.getById(id, ProcessoExecucao.class), ProcessoExecucao.class);
    }

    public List<ProcessoExecucao> listar() {
        return dao.listar(ProcessoExecucao.class);
    }
}
