package com.tcc.controller;

import com.tcc.entity.Usuario;
import com.tcc.exception.TccError;
import com.tcc.exception.TccException;
import com.tcc.exception.TccInfo;
import com.tcc.exception.TccWarning;
import com.tcc.web.auth.AutenticacaoManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

public abstract class MainController {

    @Autowired
    protected AutenticacaoManager autenticacaoManager;

    public MainController() {
    }

    public String getContextServerPath(HttpServletRequest request) {
        StringBuilder serverPath = new StringBuilder(300);
        if (request.getProtocol().startsWith("HTTPS")) {
            serverPath.append("https://");
        } else {
            serverPath.append("http://");
        }
        serverPath.append(request.getServerName());
        if (request.getServerPort() != 80) {
            serverPath.append(':').append(request.getServerPort());
        }
        serverPath.append(request.getContextPath());
        return serverPath.toString();
    }

    protected Map<String, Object> getModelMapError(String msg) {

        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("message", msg);
        modelMap.put("success", false);

        return modelMap;
    }

    protected Map<String, Object> getModelMapError(TccException te) {

        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("success", false);
        modelMap.put("message", "Operação não realizada.");
        modelMap.put("tccErrors", te.getTccErrorList());
        modelMap.put("tccInfos", te.getTccInfoList());
        modelMap.put("tccWarnings", te.getTccWarningList());

        return modelMap;
    }

    protected Map<String, Object> getModelMapError(List<TccError> errors,
            List<TccInfo> infos, List<TccWarning> warnings) {

        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("success", false);
        modelMap.put("message", "Operação não realizada.");
        modelMap.put("tccErrors", errors);
        modelMap.put("tccInfos", infos);
        modelMap.put("tccWarnings", warnings);

        return modelMap;
    }

    protected Map<String, Object> getModelMapSuccess(String msg) {

        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("message", msg);
        modelMap.put("success", true);

        return modelMap;
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Map<String, Object> tratarException(Exception exception) {
        Map<String, Object> result;
        TccException te = new TccException();
        te.addError(null, "Erro: " + exception.getMessage());
        Logger.getLogger(MainController.class).error(exception.getLocalizedMessage(), exception);
        result = getModelMapError(te.getTccErrorList(), te.getTccInfoList(), te.getTccWarningList());
        return result;
    }

    @ExceptionHandler(TccException.class)
    @ResponseBody
    public Map<String, Object> tratarTccException(TccException te) {
        Map<String, Object> result;
        result = getModelMapError(te.getTccErrorList(), te.getTccInfoList(), te.getTccWarningList());
        return result;
    }

    protected void validarUsuarioLogado() throws TccException {
        Usuario usuario = autenticacaoManager.getUsuarioLogado();
        if (usuario == null || usuario.get_id().isEmpty()) {
            TccException be = new TccException();
            be.addError(null, "Usuario não encontrado.");
            throw be;
        }
    }
}
