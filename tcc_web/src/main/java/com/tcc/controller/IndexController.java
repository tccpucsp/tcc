package com.tcc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController extends MainController {

    @RequestMapping(value = "/index")
    public String index() {
        return "index";
    }

}
