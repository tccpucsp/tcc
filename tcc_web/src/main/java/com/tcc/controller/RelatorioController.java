package com.tcc.controller;

import com.tcc.entity.Cliente;
import com.tcc.entity.Session;
import com.tcc.exception.TccException;
import com.tcc.service.RelatorioService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import vo.AcessosAnalitocoVO;
import vo.AmigoVO;
import vo.AniversarianteVO;
import vo.BrowserVO;
import vo.FaixaEtariaVO;
import vo.GeneroVO;
import vo.OsVO;
import vo.ResolutionVO;
import vo.TipoLoginVO;
import vo.TipoVO;

@Controller
public class RelatorioController extends MainController {

    @Autowired
    private RelatorioService service;

    @RequestMapping(value = "relatorio/acessosAnalitico", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> acessosAnalitico(Date dataAcesso) throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        List<Session> session = service.listarAcessosAnalitico(dataAcesso);

        List<AcessosAnalitocoVO> dataResult = new ArrayList<>();
        session.stream().forEach((d) -> {
            dataResult.add(new AcessosAnalitocoVO(d));
        });

        result = getModelMapSuccess("Registros encontrados.");
        result.put("data", dataResult);

        return result;
    }

    @RequestMapping(value = "relatorio/aniversariantes", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> aniversariantes(String dia, String mes) throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        List<Cliente> clientes = service.listarAniversariantes(dia, mes);

        List<AniversarianteVO> dataResult = new ArrayList<>();

        clientes.stream().forEach((d) -> {
            dataResult.add(new AniversarianteVO(d));
        });

        result = getModelMapSuccess("Registros encontrados.");
        result.put("data", dataResult);

        return result;
    }

    @RequestMapping(value = "relatorio/perfilDispositvosOS", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> perfilDispositvosOS() throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        List<OsVO> devicesOS = service.listarPerfilDispositvosOS();

        result = getModelMapSuccess("Registros encontrados.");
        result.put("data", devicesOS);

        return result;
    }

    @RequestMapping(value = "relatorio/perfilDispositvosTipo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> perfilDispositvosTipo() throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        List<TipoVO> deviceTipos = service.listarPerfilDispositvosTipo();

        result = getModelMapSuccess("Registros encontrados.");
        result.put("data", deviceTipos);

        return result;
    }

    @RequestMapping(value = "relatorio/perfilDispositvosBrowser", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> perfilDispositvosBrowser() throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        List<BrowserVO> deviceBrowser = service.listarPerfilDispositvosBrowser();

        result = getModelMapSuccess("Registros encontrados.");
        result.put("data", deviceBrowser);

        return result;
    }

    @RequestMapping(value = "relatorio/perfilDispositvosResolution", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> perfilDispositvosResolution() throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        List<ResolutionVO> deviceResolution = service.listarPerfilDispositvosResolution();

        result = getModelMapSuccess("Registros encontrados.");
        result.put("data", deviceResolution);

        return result;
    }

    @RequestMapping(value = "relatorio/perfilClientesTipoLogin", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> perfilClientesTipoLogin() throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        List<TipoLoginVO> clientesLogin = service.listarPerfilClientesTipoLogin();

        result = getModelMapSuccess("Registros encontrados.");
        result.put("data", clientesLogin);

        return result;
    }

    @RequestMapping(value = "relatorio/perfilClientesAmigos", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> perfilClientesAmigos() throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        List<AmigoVO> clientesAmigos = service.listarPerfilClientesAmigos();

        result = getModelMapSuccess("Registros encontrados.");
        result.put("data", clientesAmigos);

        return result;
    }

    @RequestMapping(value = "relatorio/perfilClientesGenero", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> perfilClientesGenero() throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        List<GeneroVO> clientesGenero = service.listarPerfilClientesGenero();

        result = getModelMapSuccess("Registros encontrados.");
        result.put("data", clientesGenero);

        return result;
    }

    @RequestMapping(value = "relatorio/perfilClientesFaixaEtaria", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> perfilClientesFaixaEtaria() throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        List<FaixaEtariaVO> clientesFaixaEtaria = service.listarPerfilClientesFaixaEtaria();

        result = getModelMapSuccess("Registros encontrados.");
        result.put("data", clientesFaixaEtaria);

        return result;
    }

}
