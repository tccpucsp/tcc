package com.tcc.controller;

import com.tcc.entity.Processo;
import com.tcc.entity.ProcessoExecucao;
import com.tcc.entity.ProcessoExecucaoLog;
import com.tcc.exception.TccException;
import com.tcc.service.ProcessoExecucaoLogService;
import com.tcc.service.ProcessoExecucaoService;
import com.tcc.service.ProcessoService;
import com.tcc.task.TaskManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import vo.ProcessoExecucaoLogVO;
import vo.ProcessoExecucaoVO;
import vo.ProcessoVO;

@Controller
public class GerenciadorProcessoController extends MainController {

    @Autowired
    private TaskManager taskManager;
    @Autowired
    private ProcessoService processoService;
    @Autowired
    private ProcessoExecucaoService processoExecucaoService;
    @Autowired
    private ProcessoExecucaoLogService processoExecucaoLogService;

    public GerenciadorProcessoController() {
        super();
    }

    @RequestMapping(value = "gerenciadorprocesso/obter", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> obter(String id) throws Exception {
        Map<String, Object> result;

        validarUsuarioLogado();

        Processo processo = processoService.obter(id);

        if (processo == null) {
            TccException be = new TccException();
            be.addError(null, "Nenhum registro encontrado.");
            throw be;
        }
        result = getModelMapSuccess("Registro encontrado.");
        result.put("data", processo);

        return result;
    }

    @RequestMapping(value = "gerenciadorprocesso/atualizar", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public Map<String, Object> persistir(ProcessoVO vo) throws Exception {
        Map<String, Object> result;

        validarUsuarioLogado();

        Processo p = new Processo();

        if (vo != null) {
            p.set_id(vo.getId());
            if (vo.getAtivo() == null) {
                vo.setAtivo(false);
            }
            p.setAtivo(vo.getAtivo());
            p.setCodigo(vo.getCodigo());
            p.setDataInicio(vo.getDataInicio());
            p.setDescricao(vo.getDescricao());
            p.setPeriodicidade(vo.getPeridiocidade());
        }

        processoService.persistir(p);

        //Cancelando processamentos//
        if (p.getAtivo()) {
            Boolean isCancelado = true;
            if (taskManager.getFutures().get(p.get_id()) != null) {
                isCancelado = taskManager.getFutures().get(p.get_id()).isCancelled();
            }
            if (isCancelado) {
                taskManager.agendar(taskManager.getProcessMap().get(p.get_id()));
            }
        } else {
            taskManager.cancel(p.get_id());
        }

        result = getModelMapSuccess("Operação realizada com sucesso!");
        result.put("data", vo);

        return result;
    }

    @RequestMapping(value = "gerenciadorprocesso/agendarprocesso", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public Map<String, Object> agendarProcesso(String id) throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        if (id == null || id.isEmpty()) {
            TccException te = new TccException();
            te.addError(null, "Nenhum processo foi informado!");
            throw te;
        }

        taskManager.agendar(taskManager.getProcessMap().get(id));

        result = getModelMapSuccess("Operação realizada com sucesso!");
        return result;
    }

    @RequestMapping(value = "gerenciadorprocesso/executarprocesso", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public Map<String, Object> executarProcesso(String id) throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        if (id == null || id.isEmpty()) {
            TccException te = new TccException();
            te.addError(null, "Nenhum processo foi informado!");
            throw te;
        }

        Boolean resultado = taskManager.executar(id);

        result = getModelMapSuccess("Operação realizada com sucesso!");
        result.put("data", resultado);
        return result;
    }

    @RequestMapping(value = "gerenciadorprocesso/executarprocessos", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public Map<String, Object> executarProcessos(String[] ids) throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        if (ids == null || ids.length <= 0) {
            TccException be = new TccException();
            be.addError(null, "Nenhum registro informado para cancelamento.");
            throw be;
        }

        Boolean resultado = false;

        for (String id : ids) {
            if (id != null && !id.isEmpty()) {
                resultado = taskManager.executar(id);
            }
        }

        result = getModelMapSuccess("Operação realizada com sucesso!");
        result.put("data", resultado);
        return result;
    }

    @RequestMapping(value = "gerenciadorprocesso/cancelarprocessos", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public Map<String, Object> cancelarProcessos(String[] ids) throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        if (ids == null || ids.length <= 0) {
            TccException be = new TccException();
            be.addError(null, "Nenhum registro informado para cancelamento.");
            throw be;
        }

        Boolean resultado = false;

        for (String id : ids) {
            if (id != null && !id.isEmpty()) {
                resultado = taskManager.cancel(id);
            }
        }

        result = getModelMapSuccess("Operação realizada com sucesso!");
        result.put("data", resultado);
        return result;
    }

    @RequestMapping(value = "gerenciadorprocesso/pararprocessos", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public Map<String, Object> pararProcessos(String[] ids) throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        if (ids == null || ids.length <= 0) {
            TccException be = new TccException();
            be.addError(null, "Nenhum registro informado para parar.");
            throw be;
        }

        Boolean resultado = false;

        for (String id : ids) {
            if (id != null && !id.isEmpty()) {
                resultado = taskManager.cancelAndStop(id);
            }
        }

        result = getModelMapSuccess("Operação realizada com sucesso!");
        result.put("data", resultado);
        return result;
    }

    @RequestMapping(value = "gerenciadorprocesso/removerprocessos", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public Map<String, Object> removerProcesso(String[] ids) throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        if (ids == null || ids.length <= 0) {
            TccException be = new TccException();
            be.addError(null, "Nenhum registro informado para remover.");
            throw be;
        }

        Boolean resultado = false;

        for (String id : ids) {
            if (id != null && !id.isEmpty()) {
                resultado = taskManager.remove(id);
            }
        }

        result = getModelMapSuccess("Operação realizada com sucesso!");
        result.put("data", resultado);
        return result;
    }

    @RequestMapping(value = "gerenciadorprocesso/listarprocessos", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> listarProcessos() throws Exception {
        Map<String, Object> result;

        validarUsuarioLogado();

        List<Processo> lista = processoService.listar();

        List<ProcessoVO> listaVO = new ArrayList<>();

        lista.stream().forEach((item) -> {
            listaVO.add(new ProcessoVO(item));
        });

        result = getModelMapSuccess("Registros encontrados.");
        result.put("data", listaVO);

        return result;
    }

    @RequestMapping(value = "gerenciadorprocesso/listarexecucoes", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> listarExecucoes() throws Exception {
        Map<String, Object> result;

        validarUsuarioLogado();

        List<ProcessoExecucao> lista = processoExecucaoService.listar();

        List<ProcessoExecucaoVO> listaVO = new ArrayList<>();

        lista.stream().forEach((item) -> {
            listaVO.add(new ProcessoExecucaoVO(item));
        });

        result = getModelMapSuccess("Registros encontrados.");
        result.put("data", listaVO);

        return result;
    }

    @RequestMapping(value = "gerenciadorprocesso/listarlogs", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> listarLogs() throws Exception {
        Map<String, Object> result;

        validarUsuarioLogado();

        List<ProcessoExecucaoLog> lista = processoExecucaoLogService.listar();

        List<ProcessoExecucaoLogVO> listaVO = new ArrayList<>();

        lista.stream().forEach((item) -> {
            listaVO.add(new ProcessoExecucaoLogVO(item));
        });

        result = getModelMapSuccess("Registros encontrados.");
        result.put("data", listaVO);

        return result;
    }

}
