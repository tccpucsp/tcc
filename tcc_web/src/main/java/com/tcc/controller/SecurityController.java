package com.tcc.controller;

import com.tcc.cript.base.Cripter;
import com.tcc.entity.Usuario;
import com.tcc.exception.TccException;
import com.tcc.service.UsuarioService;
import com.tcc.web.auth.AutenticacaoSO;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SecurityController extends MainController {

    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private AutenticacaoSO autenticacaoSO;
    @Autowired
    private Cripter cripter;

    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> authentication(String j_username, String j_password,
            HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

        Map<String, Object> result = getModelMapError("Ocorreu um problema com a operação, por favor entre em contato com o Administrador do sistema.");

        try {
            Usuario usuario = usuarioService.loadUserByUsername(j_username.trim());
            if (usuario != null) {

                if (usuario.getPassword().equals(cripter.get(j_password))) {
                    autenticacaoSO.setDataLogin(new Date());
                    autenticacaoSO.setUsuarioLogadoId(usuario.get_id());
                    autenticacaoSO.setUsuarioNome(usuario.getName());

                    result = getModelMapSuccess("Usuário autenticado com sucesso.");
                    result.put("nomeUsuario", usuario.getName());
                } else {
                    result = getModelMapError("Usuário e/ou senha inválido(s).");
                }
            } else {
                result = getModelMapError("Usuário e/ou senha inválido(s).");
            }

        } catch (TccException te) {
            if (te.getTccErrorList().isEmpty()) {
                result = getModelMapError(te);
            } else {
                Logger.getLogger(SecurityController.class.getName()).log(Level.SEVERE, null, te);
            }
        } catch (Exception e) {
            Logger.getLogger(SecurityController.class.getName()).log(Level.SEVERE, null, e);
        }

        return result;
    }

    @RequestMapping(value = "/auth/logged", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> logged() {
        Map<String, Object> result;

        try {

            if (autenticacaoSO.getUsuarioLogadoId() != null && !autenticacaoSO.getUsuarioLogadoId().isEmpty()) {
                result = getModelMapSuccess("Usuário autenticado.");
                result.put("nomeUsuario", autenticacaoSO.getUsuarioNome());
            } else {
                result = getModelMapError("Usuário não autenticado.");
                result.put("error", "Usuário não autenticado, por favor efetue login no sistema.");
            }
        } catch (Exception e) {
            result = getModelMapError(e.getMessage());
        }

        return result;
    }

    @RequestMapping(value = "/exc", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> exclude(HttpSession session) throws Exception {

        Map<String, Object> result;

        autenticacaoSO.invalidar();
        session.invalidate();
        result = getModelMapSuccess("Logout realizado com sucesso!");

        return result;
    }

    @RequestMapping(value = "esqueciMinhaSenha", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> esqueciMinhaSenha(String login, HttpServletRequest request) throws Exception {

        Map<String, Object> result;

        Usuario u = usuarioService.getUserNewPass(login);

        String novaSenha = u.getPassword();

        u.setPassword(cripter.get(novaSenha));

        usuarioService.persistir(u);

        result = getModelMapSuccess("Operação realizada com sucesso! \n Nova senha: " + novaSenha);

        return result;
    }
}
