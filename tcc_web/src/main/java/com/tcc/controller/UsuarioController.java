package com.tcc.controller;

import com.tcc.cript.base.Cripter;
import com.tcc.entity.Usuario;
import com.tcc.exception.TccException;
import com.tcc.service.UsuarioService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import vo.UsuarioVO;

@Controller
public class UsuarioController extends MainController {

    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private Cripter cripter;

    @RequestMapping(value = "/usuario/listar", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> listar() throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        List<Usuario> usuarios = usuarioService.listar();

        List<UsuarioVO> usuResult = new ArrayList<>();
        usuarios.stream().forEach((u) -> {
            usuResult.add(new UsuarioVO(u));
        });

        result = getModelMapSuccess("Registros encontrados.");
        result.put("data", usuResult);

        return result;
    }

    @RequestMapping(value = "/usuario/obter", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> obter(String id) throws TccException {
        Map<String, Object> result;

        validarUsuarioLogado();

        Usuario u = usuarioService.getById(id);

        result = getModelMapSuccess("Registro encontrado.");
        result.put("data", new UsuarioVO(u));

        return result;
    }

    @RequestMapping(value = "/usuario/persistir", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> persistir(UsuarioVO vo) throws Exception {
        Map<String, Object> result;

        validarUsuarioLogado();

        Usuario u = null;

        if (vo != null) {
            u = new Usuario();
            u.setEmail(vo.getEmail());
            u.setName(vo.getName());
            u.set_id(vo.getId());
            if (vo.getPassword() != null && !vo.getPassword().isEmpty()) {
                u.setPassword(cripter.get(vo.getPassword()));
            }
            u.setUsername(vo.getUsername());
        }
        usuarioService.persistir(u);

        result = getModelMapSuccess("Operação realizada com sucesso.");

        return result;
    }

    @RequestMapping(value = "/usuario/remover", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> remover(String[] ids) throws Exception {
        Map<String, Object> result;

        validarUsuarioLogado();

        for (String id : ids) {

            usuarioService.remover(id);
        }

        result = getModelMapSuccess("Operação realizada com sucesso.");

        return result;
    }

}
