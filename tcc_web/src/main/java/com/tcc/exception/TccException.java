package com.tcc.exception;

import java.util.ArrayList;
import java.util.List;

public class TccException extends Exception {

    private static final long serialVersionUID = 1L;
    private List<TccError> tccErrorList;
    private List<TccInfo> tccInfoList;
    private List<TccWarning> tccWarningList;

    public TccException() {
        super();
        this.tccErrorList = new ArrayList<>();
        this.tccInfoList = new ArrayList<>();
        this.tccWarningList = new ArrayList<>();
    }

    public List<TccError> getTccErrorList() {
        return tccErrorList;
    }

    public List<TccInfo> getTccInfoList() {
        return tccInfoList;
    }

    public List<TccWarning> getTccWarningList() {
        return tccWarningList;
    }

    public boolean hasError() {

        return tccErrorList.size() > 0;
    }

    public boolean hasInfo() {
        return tccInfoList.size() > 0;
    }

    public boolean hasWarning() {
        return tccWarningList.size() > 0;
    }

    public void addError(String code, String message) {
        tccErrorList.add(new TccError(code, message));
    }

    public void addInfo(String code, String message) {
        tccInfoList.add(new TccInfo(code, message));
    }

    public void addWarning(String code, String message) {
        tccWarningList.add(new TccWarning(code, message));
    }

    public void limparErrors() {
        tccErrorList.clear();
    }

    public void limparInfos() {
        tccInfoList.clear();
    }

    public void limparWarnings() {
        tccWarningList.clear();
    }

    public void limparTudo() {
        limparErrors();
        limparInfos();
        limparWarnings();
    }

    public boolean isEmpty() {

        boolean isEmpty = true;

        if (hasError()) {
            isEmpty = false;
        }
        if (hasInfo()) {
            isEmpty = false;
        }
        if (hasWarning()) {
            isEmpty = false;
        }
        return isEmpty;
    }
}
