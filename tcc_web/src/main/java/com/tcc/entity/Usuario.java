package com.tcc.entity;

import org.bson.Document;

public class Usuario extends GenericEntity {

    private String username;
    private String password;
    private String name;
    private String email;

    public Usuario() {
    }

    public Usuario(String _id) {
        super(_id);
    }

    @Override
    public void fullConstruct(Document document) {
        this._id = document.get("_id") == null ? null : document.get("_id").toString();
        this.username = document.get("username") == null ? null : document.get("username").toString();
        this.password = document.get("password") == null ? null : document.get("password").toString();
        this.name = document.get("name") == null ? null : document.get("name").toString();
        this.email = document.get("email") == null ? null : document.get("email").toString();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
