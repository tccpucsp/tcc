package com.tcc.entity;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;

public class ClienteFacebook extends Agregado {

    /**
     * facebook_key
     */
    private String id;
    private String token;
    private String email;
    private String name;
    private List<ClienteFacebookAmigo> friendList;

    public ClienteFacebook() {
    }

    public ClienteFacebook(Document document) {

        this.id = document.get("id") != null ? document.get("id").toString() : null;
        this.token = document.get("token") != null ? document.get("token").toString() : null;
        this.email = document.get("email") != null ? document.get("email").toString() : null;
        this.name = document.get("name") != null ? document.get("name").toString() : null;
        if (document.get("friendList") != null) {

            List<Document> fList = (List<Document>) document.get("friendList");

            List<ClienteFacebookAmigo> amigos = new ArrayList<>();

            for (Document f : fList) {
                amigos.add(new ClienteFacebookAmigo(f));
            }

            this.friendList = amigos;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ClienteFacebookAmigo> getFriendList() {
        return friendList;
    }

    public void setFriendList(List<ClienteFacebookAmigo> friendList) {
        this.friendList = friendList;
    }

    @Override
    public Document serializar() {
        Document doc = new Document();

        doc.append("id", this.id);
        doc.append("token", this.token);
        doc.append("email", this.email);
        doc.append("name", this.name);
        doc.append("friendList", this.serializarFriendList());

        return doc;
    }

    public List<Document> serializarFriendList() {

        List<Document> friends = new ArrayList<>();

        this.friendList.stream().forEach((f) -> {
            friends.add(f.serializar());
        });

        return friends;

    }

    @Override
    public void fullConstruct(Object obj) {

        ClienteFacebook cf = (ClienteFacebook) obj;

        this.id = cf.getId();
        this.token = cf.getToken();
        this.email = cf.getEmail();
        this.name = cf.getName();
        this.friendList = cf.getFriendList();
    }

}
