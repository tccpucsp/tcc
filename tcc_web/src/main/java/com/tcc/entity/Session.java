package com.tcc.entity;

import com.tcc.utils.converters.DateConverter;
import java.util.Date;
import org.bson.Document;

public class Session extends GenericEntity {

    private String device_mac;
    private String meraki_id;
    private String ip;
    private String ap_mac_ssid;
    private Date start;
    private Date stop;

    public Session() {
    }

    public Session(String _id) {
        super(_id);
    }

    @Override
    public void fullConstruct(Document document) {
        this._id = document.get("_id") != null ? document.get("_id").toString() : null;
        this.device_mac = document.get("device_mac") != null ? document.get("device_mac").toString() : null;
        this.meraki_id = document.get("meraki_id") != null ? document.get("meraki_id").toString() : null;
        this.ip = document.get("ip") != null ? document.get("ip").toString() : null;
        this.ap_mac_ssid = document.get("ap_mac_ssid") != null ? document.get("ap_mac_ssid").toString() : null;
        this.start = document.get("start").toString().isEmpty() ? null : DateConverter.dateParse(document.get("start").toString());
        this.stop = document.get("stop").toString().isEmpty() ? null : DateConverter.dateParse(document.get("stop").toString());
    }

    public String getDevice_mac() {
        return device_mac;
    }

    public void setDevice_mac(String device_mac) {
        this.device_mac = device_mac;
    }

    public String getMeraki_id() {
        return meraki_id;
    }

    public void setMeraki_id(String meraki_id) {
        this.meraki_id = meraki_id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getAp_mac_ssid() {
        return ap_mac_ssid;
    }

    public void setAp_mac_ssid(String ap_mac_ssid) {
        this.ap_mac_ssid = ap_mac_ssid;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getStop() {
        return stop;
    }

    public void setStop(Date stop) {
        this.stop = stop;
    }

}
