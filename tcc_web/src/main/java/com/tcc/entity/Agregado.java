package com.tcc.entity;

import org.bson.Document;

public abstract class Agregado {

    public abstract Document serializar();

    public abstract void fullConstruct(Object obj);

}
