package com.tcc.entity;

import com.tcc.utils.converters.DateConverter;
import java.util.Date;
import org.bson.Document;

public class ProcessoExecucaoLog extends GenericEntity {

    private ProcessoExecucao execucao;
    private String log;
    private Date data;

    public ProcessoExecucaoLog() {
    }

    public ProcessoExecucaoLog(String _id, ProcessoExecucao execucao, String log, Date data) {
        super(_id);
        this.execucao = execucao;
        this.log = log;
        this.data = data;
    }

    @Override
    public void fullConstruct(Document document) {
        this._id = document.get("_id") != null ? document.get("_id").toString() : null;

       
        this.log = document.get("log") != null ? document.get("log").toString() : null;
        this.data = document.get("data") != null ? document.get("data").toString().isEmpty() ? null : DateConverter.dateParse(document.get("data").toString()) : null;
    }

    public ProcessoExecucao getExecucao() {
        return execucao;
    }

    public void setExecucao(ProcessoExecucao execucao) {
        this.execucao = execucao;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
