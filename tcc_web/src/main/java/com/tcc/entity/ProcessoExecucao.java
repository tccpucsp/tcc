package com.tcc.entity;

import com.tcc.utils.converters.DateConverter;
import java.util.Date;
import org.bson.Document;

public class ProcessoExecucao extends GenericEntity {

    private Processo processo;
    private Date dataInicio;
    private Date dataFinal;
    private String situacao;

    public ProcessoExecucao() {
    }

    @Override
    public void fullConstruct(Document document) {
        this._id = document.get("_id") != null ? document.get("_id").toString() : null;

        if (document.get("processo") != null) {
            Processo p = new Processo();
            p.set_id(document.get("processo").toString());
            this.processo = p;
        }

        this.dataInicio = document.get("dataInicio") != null ? document.get("dataInicio").toString().isEmpty() ? null : DateConverter.dateParse(document.get("dataInicio").toString()) : null;
        this.dataFinal = document.get("dataFinal") != null ? document.get("dataFinal").toString().isEmpty() ? null : DateConverter.dateParse(document.get("dataFinal").toString()) : null;
        this.situacao = document.get("situacao") != null ? document.get("situacao").toString() : null;
    }

    public Processo getProcesso() {
        return processo;
    }

    public void setProcesso(Processo processo) {
        this.processo = processo;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }
}
