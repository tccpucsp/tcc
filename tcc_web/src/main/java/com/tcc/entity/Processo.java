package com.tcc.entity;

import com.tcc.utils.converters.DateConverter;
import java.util.Date;
import org.bson.Document;

public class Processo extends GenericEntity {

    private String codigo;
    private String descricao;
    private String periodicidade;
    private Boolean ativo = false;
    private Date dataInicio;

    public Processo() {
    }

    public Processo(String _id) {
        super(_id);
    }

    @Override
    public void fullConstruct(Document document) {

        this._id = document.get("_id") != null ? document.get("_id").toString() : null;
        this.codigo = document.get("codigo") != null ? document.get("codigo").toString() : null;
        this.descricao = document.get("descricao") != null ? document.get("descricao").toString() : null;
        this.periodicidade = document.get("periodicidade") != null ? document.get("periodicidade").toString() : null;
        this.ativo = document.get("ativo") != null ? "true".equals(document.get("ativo").toString()) : null;
        this.dataInicio = document.get("dataInicio") != null && !document.get("dataInicio").toString().isEmpty()
                ? DateConverter.dateParse(document.get("dataInicio").toString()) : null;
    }

    public Processo(String id, String codigo, String descricao,
            String periodicidade, Boolean ativo, Date dataInicio) {
        super();
        this.codigo = codigo;
        this.descricao = descricao;
        this.periodicidade = periodicidade;
        this.ativo = ativo;
        this.dataInicio = dataInicio;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getPeriodicidade() {
        return periodicidade;
    }

    public void setPeriodicidade(String periodicidade) {
        this.periodicidade = periodicidade;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }
}
