package com.tcc.entity;

import org.bson.Document;

public class ClienteFacebookAmigo extends Agregado {

    /**
     * facebook_key
     */
    private String id;
    private String name;

    public ClienteFacebookAmigo() {
    }

    public ClienteFacebookAmigo(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public ClienteFacebookAmigo(Document document) {
        this.id = document.get("id").toString();
        this.name = document.get("name").toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Document serializar() {

        Document doc = new Document();

        doc.append("id", this.id);
        doc.append("name", this.name);

        return doc;
    }

    @Override
    public void fullConstruct(Object obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
