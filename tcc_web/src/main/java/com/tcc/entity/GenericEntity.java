package com.tcc.entity;

import org.bson.Document;

public abstract class GenericEntity {

    protected String _id;

    protected GenericEntity() {
    }

    protected GenericEntity(String _id) {
        this._id = _id;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public abstract void fullConstruct(Document dbo);
}
