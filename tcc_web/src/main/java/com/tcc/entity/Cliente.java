package com.tcc.entity;

import com.tcc.utils.converters.DateConverter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.bson.Document;

public class Cliente extends GenericEntity {

    private String email;
    private String username;
    private String password;
    private String doc;
    private Boolean is_cpf;
    private Boolean active;
    private String name;
    private String last_name;
    private Boolean gender;
    private String phone;
    private Date birthday;
    private Boolean opt;
    private Date created_at;
    private Date deleted_at;
    private String origin;
    private String origin_local;
    private Boolean logged;
    private String type_login;
    private List<Device> device;
    private ClienteFacebook clienteFacebook;

    public Cliente() {
    }

    public Cliente(String _id) {
        super(_id);
    }

    @Override
    public void fullConstruct(Document document) {

        this._id = document.get("_id") != null ? document.get("_id").toString() : null;
        this.email = document.get("email") != null ? document.get("email").toString() : null;
        this.username = document.get("username") != null ? document.get("username").toString() : null;
        this.password = document.get("password") != null ? document.get("password").toString() : null;
        this.doc = document.get("doc") != null ? document.get("doc").toString() : null;
        this.is_cpf = document.get("is_cpf") != null ? "true".equals(document.get("is_cpf").toString()) : null;
        this.active = document.get("active") != null ? "true".equals(document.get("active").toString()) : null;
        this.name = document.get("name") != null ? document.get("name").toString() : null;
        this.last_name = document.get("lastname") != null ? document.get("lastname").toString() : null;
        String g = document.get("gender").toString();
        this.gender = document.get("gender") != null ? "Male".equals(g) : null;
        this.phone = document.get("phone") != null ? document.get("phone").toString() : null;
        this.opt = document.get("opt") != null ? "true".equals(document.get("opt").toString()) : null;
        this.origin = document.get("origin") != null ? document.get("origin").toString() : null;
        this.origin_local = document.get("origin_local") != null ? document.get("origin_local").toString() : null;
        this.logged = document.get("logged") != null ? "true".equals(document.get("logged").toString()) : null;
        this.type_login = document.get("typeLogin") != null ? document.get("typeLogin").toString() : null;

        this.created_at = document.get("created_at") != null && !document.get("created_at").toString().isEmpty()
                ? DateConverter.dateParse(document.get("created_at").toString()) : null;

        this.deleted_at = document.get("deleted_at") != null && !document.get("deleted_at").toString().isEmpty()
                ? DateConverter.dateParse(document.get("deleted_at").toString()) : null;

        this.birthday = document.get("birthday") != null && !document.get("birthday").toString().isEmpty()
                ? DateConverter.dateParse(document.get("birthday").toString()) : null;

        if (document.get("device") != null) {

            List<Document> dList = (List<Document>) document.get("device");

            List<Device> devices = new ArrayList<>();
            dList.stream().forEach((d) -> {
                devices.add(new Device(d));
            });

            this.device = devices;
        }

        this.clienteFacebook = document.get("clienteFacebook") != null ? new ClienteFacebook((Document) document.get("clienteFacebook")) : null;

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public Boolean getIs_cpf() {
        return is_cpf;
    }

    public void setIs_cpf(Boolean is_cpf) {
        this.is_cpf = is_cpf;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Boolean getOpt() {
        return opt;
    }

    public void setOpt(Boolean opt) {
        this.opt = opt;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(Date deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getOrigin_local() {
        return origin_local;
    }

    public void setOrigin_local(String origin_local) {
        this.origin_local = origin_local;
    }

    public Boolean getLogged() {
        return logged;
    }

    public void setLogged(Boolean logged) {
        this.logged = logged;
    }

    public String getType_login() {
        return type_login;
    }

    public void setType_login(String type_login) {
        this.type_login = type_login;
    }

    public List<Device> getDevice() {
        return device;
    }

    public void setDevice(List<Device> device) {
        this.device = device;
    }

    public ClienteFacebook getClienteFacebook() {
        return clienteFacebook;
    }

    public void setClienteFacebook(ClienteFacebook clienteFacebook) {
        this.clienteFacebook = clienteFacebook;
    }

}
