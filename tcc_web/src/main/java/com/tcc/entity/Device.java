package com.tcc.entity;

import com.mongodb.BasicDBObject;
import org.bson.Document;

public class Device extends Agregado {

    private String mac;
    private String name;
    private String type;
    private String resolution;
    private String os;
    private String browser;
    private Boolean is_mobile;

    public Device() {
    }

    public Device(Document document) {
        this.mac = document.get("mac") != null ? document.get("mac").toString() : null;
        this.name = document.get("name") != null ? document.get("name").toString() : null;
        this.type = document.get("type") != null ? document.get("type").toString() : null;
        this.resolution = document.get("resolution") != null ? document.get("resolution").toString() : null;
        this.os = document.get("os") != null ? document.get("os").toString() : null;
        this.browser = document.get("browser") != null ? document.get("browser").toString() : null;
        this.is_mobile = document.get("is_mobile") != null ? "true".equals(document.get("is_mobile").toString()) : null;
    }

    public Device(BasicDBObject dbObj) {
        this.mac = dbObj.get("mac") != null ? dbObj.get("mac").toString() : null;
        this.name = dbObj.get("name") != null ? dbObj.get("name").toString() : null;
        this.type = dbObj.get("type") != null ? dbObj.get("type").toString() : null;
        this.resolution = dbObj.get("resolution") != null ? dbObj.get("resolution").toString() : null;
        this.os = dbObj.get("os") != null ? dbObj.get("os").toString() : null;
        this.browser = dbObj.get("browser") != null ? dbObj.get("browser").toString() : null;
        this.is_mobile = dbObj.get("is_mobile") != null ? "true".equals(dbObj.get("is_mobile").toString()) : null;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public Boolean getIs_mobile() {
        return is_mobile;
    }

    public void setIs_mobile(Boolean is_mobile) {
        this.is_mobile = is_mobile;
    }

    @Override
    public Document serializar() {

        Document doc = new Document();

        doc.append("mac", this.mac);
        doc.append("name", this.name);
        doc.append("type", this.type);
        doc.append("resolution", this.resolution);
        doc.append("os", this.os);
        doc.append("browser", this.browser);
        doc.append("is_mobile", this.is_mobile);

        return doc;
    }

    @Override
    public void fullConstruct(Object obj) {
        Device d = (Device) obj;

        this.mac = d.getMac();
        this.name = d.getName();
        this.type = d.getType();
        this.resolution = d.getResolution();
        this.os = d.getOs();
        this.browser = d.getBrowser();
        this.is_mobile = d.getIs_mobile();
    }

}
