/* global Ext */

Ext.define('resources.js.tcc.utils.emailField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.emailfield',
    fieldLabel: 'Email',
    labelAlign: 'right',
    vtype: 'email',
    initComponent: function () {
        var me = this;

        Ext.apply(me, {});

        me.callParent();
    }
});

