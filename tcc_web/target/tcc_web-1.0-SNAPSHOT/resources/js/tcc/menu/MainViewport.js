/* global Ext */

Ext.define('resources.js.tcc.menu.MainViewport', {
    extend: 'Ext.container.Viewport',
    alias: 'widget.mainviewport',
    layout: {
        type: 'card'
    },
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            renderTo: Ext.getBody(),
            items: [
                {
                    xtype: 'container',
                    itemId: 'loading',
                    layout: {
                        type: 'border'
                    },
                    items: [
                        {
                            xtype: 'container',
                            region: 'center',
                            itemId: 'load',
                            style: 'background-color: white',
                            layout: {
                                type: 'fit'
                            }
                        }
                    ]
                },
                {
                    xtype: 'container',
                    itemId: 'login',
                    layout: {
                        type: 'border'
                    },
                    items: [
                        {
                            xtype: 'container',
                            region: 'center',
                            html: '<center><p><br/><br/><img style="margin-top:100px;" alt="" width="500px" heigth="175px" src="resources/imagens/logo.png"></p><br/><br/><br/><br/><div id="login"></div><p><br/><a id="esqueciMinhaSenha" href="javascript:void(0);">Esqueci minha senha</a><br/><br/></p><div id="createNewPassword"></div></center>',
                            itemId: 'principal',
                            style: 'background-color: white',
                            layout: {
                                type: 'fit'
                            }
                        }
                    ]
                },
                {
                    xtype: 'container',
                    itemId: 'main',
                    style: 'background-color: #263238; ',
                    layout: {
                        type: 'border'
                    },
                    items: [
                        {
                            xtype: 'container',
                            region: 'center',
                            itemId: 'conteudo',
                            style: 'background-color: white; border-style: solid; border-width: 1px; border-color: #99CCFF;',
                            margins: '5 5 5 5',
                            layout: {
                                type: 'fit'
                            },
                            items: [
                            ]
                        },
                        {
                            xtype: 'container',
                            region: 'west',
                            itemId: 'menuContainer',
                            margins: '5 5 5 5',
                            layout: {
                                type: 'vbox',
                                pack: 'center'
                            },
                            items: []
                        },
                        {
                            xtype: 'container',
                            region: 'south',
                            height: 15,
                            itemId: 'rodape',
                            margins: '5 5 5 5',
                            layout: {
                                align: 'stretch',
                                type: 'hbox'
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    html: '<marquee scrollamount="4"><font color="#FFFFFF"> Grupo TCC: Felipe Silva Pereira, Fernanda Roberta da Silva, Flávia de Aragão Santos, Rafael de Arruda</font></marquee>'
                                },
                                {
                                    xtype: 'label',
                                    html: '<font size="0.5" color="#FFFFFF"> <b> TCC PUC-SP - 2015 </b> - <i> Projeto: Quem Visita</i>&nbsp;</font>'
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});