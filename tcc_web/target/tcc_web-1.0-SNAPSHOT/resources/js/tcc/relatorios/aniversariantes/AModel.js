/* global Ext */

Ext.define('resources.js.tcc.relatorios.aniversariantes.AModel', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id'
        },
        {
            name: 'email'
        },
        {
            name: 'name'
        },
        {
            name: 'last_name'
        },
        {
            name: 'gender',
            convert: function (value, record) {

                if (value === true) {
                    return "Masculino";
                } else {
                    return "Feminino";
                }
            }
        },
        {
            name: 'phone'
        },
        {
            name: 'birthday',
            dateFormat: 'time',
            type: 'date'
        },
        {
            name: 'idade',
            convert: function (value, record) {

                var today = new Date();
                var birthDate = record.get('birthday');
                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }
                return age;
            }

        }
    ]
});
