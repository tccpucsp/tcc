/* global Ext */

Ext.define('resources.js.tcc.relatorios.perfildispositivos.TipoModel', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'tipo'
        },
        {
            name: 'total'
        }
    ]
});
