/* global Ext */

Ext.define('resources.js.tcc.relatorios.perfildispositivos.listagem.ListagemView', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.relatoriosperfildispositivoslistagemview',
    requires: [
        'resources.js.tcc.relatorios.perfildispositivos.OSModel',
        'resources.js.tcc.relatorios.perfildispositivos.TipoModel',
        'resources.js.tcc.relatorios.perfildispositivos.BrowserModel',
        'resources.js.tcc.relatorios.perfildispositivos.ResolutionModel'
    ],
    header: {style: 'background-color: #0277bd;'},
    layout: {
        type: 'fit'
    },
    title: 'Perfil de Dispositivos',
    iconCls: 'iconDispositivo',
    itemId: 'pDispositivos',
    border: true,
    initComponent: function () {
        var me = this;

        var osStore = Ext.create('Ext.data.JsonStore', {
            model: 'resources.js.tcc.relatorios.perfildispositivos.OSModel',
            proxy: {
                type: 'ajax',
                url: 'relatorio/perfilDispositvosOS',
                reader: {
                    type: 'json'
                }
            }
        });

        var tipoStore = Ext.create('Ext.data.JsonStore', {
            model: 'resources.js.tcc.relatorios.perfildispositivos.TipoModel',
            proxy: {
                type: 'ajax',
                url: 'relatorio/perfilDispositvosTipo',
                reader: {
                    type: 'json'
                }
            }
        });

        var browserStore = Ext.create('Ext.data.JsonStore', {
            model: 'resources.js.tcc.relatorios.perfildispositivos.BrowserModel',
            proxy: {
                type: 'ajax',
                url: 'relatorio/perfilDispositvosBrowser',
                reader: {
                    type: 'json'
                }
            }
        });

        var resolutionStore = Ext.create('Ext.data.JsonStore', {
            model: 'resources.js.tcc.relatorios.perfildispositivos.ResolutionModel',
            proxy: {
                type: 'ajax',
                url: 'relatorio/perfilDispositvosResolution',
                reader: {
                    type: 'json'
                }
            }
        });

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    title: 'Sistema Operacional',
                    layout: {
                        type: 'fit'
                    },
                    items: [
                        {
                            xtype: 'chart',
                            itemId: 'chartSO',
                            animate: true,
                            insetPadding: 20,
                            shadow: true,
                            legend: {
                                position: 'right'
                            },
                            theme: 'Category3',
                            store: osStore,
                            series: [
                                {
                                    type: 'pie',
                                    showInLegend: true,
                                    angleField: 'total',
                                    tips: {
                                        trackMouse: true,
                                        width: 140,
                                        height: 28,
                                        renderer: function (storeItem, item) {

                                            var total = 0;
                                            osStore.each(function (rec) {
                                                total += rec.get('total');
                                            });
                                            this.setTitle(storeItem.get('os') + ': ' + Math.round(storeItem.get('total') / total * 100) + '%');
                                        }
                                    },
                                    highlight: {
                                        segment: {
                                            margin: 20
                                        }
                                    },
                                    label: {
                                        field: 'os',
                                        display: 'rotate',
                                        contrast: true,
                                        font: '15px Arial'
                                    }
                                }
                            ]
                        }
                    ],
                    tabConfig: {
                        xtype: 'tab',
                        flex: 1,
                        minWidth: 230
                    }
                },
                {
                    xtype: 'form',
                    title: 'Tipo',
                    layout: {
                        type: 'fit'
                    },
                    items: [
                        {
                            xtype: 'chart',
                            itemId: 'chartTipo',
                            style: 'background: #fff',
                            animate: true,
                            shadow: false,
                            store: tipoStore,
                            insetPadding: 40,
                            axes: [{
                                    title: 'Mobile - Normal',
                                    type: 'gauge',
                                    position: 'gauge',
                                    steps: 4,
                                    margin: 10,
                                    label: {
                                        renderer: function (v) {
                                            return v;
                                        }
                                    }
                                }],
                            series: [{
                                    type: 'gauge',
                                    field: 'total',
                                    donut: 50
                                }]
                        }
                    ],
                    tabConfig: {
                        xtype: 'tab',
                        flex: 1,
                        minWidth: 230
                    }
                },
                {
                    xtype: 'form',
                    title: 'Resolução',
                    layout: {
                        type: 'fit'
                    },
                    items: [
                        {
                            xtype: 'chart',
                            itemId: 'chartResolution',
                            style: 'background: #fff',
                            theme: 'Category6',
                            insetPadding: 40,
                            animate: true,
                            shadow: false,
                            store: resolutionStore,
                            axes: [
                                {
                                    type: 'Numeric',
                                    position: 'left',
                                    fields: ['total'],
                                    label: {
                                        renderer: function (v) {
                                            return v;
                                        }
                                    },
                                    grid: true,
                                    minimum: 0
                                },
                                {
                                    type: 'Category',
                                    position: 'bottom',
                                    fields: ['resolution'],
                                    grid: true,
                                    label: {
                                        rotate: {
                                            degrees: -45
                                        }
                                    }
                                }
                            ],
                            series: [
                                {
                                    type: 'column',
                                    axis: 'left',
                                    xField: 'resolution',
                                    yField: 'total',
                                    style: {
                                        opacity: 0.80
                                    },
                                    highlight: {
                                        fill: '#000',
                                        'stroke-width': 20,
                                        stroke: '#fff'
                                    },
                                    tips: {
                                        trackMouse: true,
                                        style: 'background: #FFF',
                                        height: 20,
                                        renderer: function (storeItem, item) {
                                            this.setTitle(storeItem.get('total'));
                                        }
                                    }
                                }
                            ]
                        }
                    ],
                    tabConfig: {
                        xtype: 'tab',
                        flex: 1,
                        minWidth: 230
                    }
                },
                {
                    xtype: 'form',
                    title: 'Browser',
                    layout: {
                        type: 'fit'
                    },
                    items: [
                        {
                            xtype: 'chart',
                            itemId: 'chartBrowser',
                            style: 'background: #fff',
                            animate: true,
                            shadow: false,
                            theme: 'Purple',
                            store: browserStore,
                            insetPadding: 40,
                            axes: [
                                {
                                    type: 'Numeric',
                                    position: 'bottom',
                                    fields: ['total'],
                                    label: {
                                        renderer: function (v) {
                                            return v;
                                        }
                                    },
                                    grid: true,
                                    minimum: 0
                                },
                                {
                                    type: 'Category',
                                    position: 'left',
                                    fields: ['browser'],
                                    grid: true
                                }
                            ],
                            series: [
                                {
                                    type: 'bar',
                                    axis: 'bottom',
                                    xField: 'browser',
                                    yField: 'total',
                                    style: {
                                        opacity: 0.80
                                    },
                                    highlight: {
                                        fill: '#000',
                                        'stroke-width': 2,
                                        stroke: '#fff'
                                    },
                                    tips: {
                                        trackMouse: true,
                                        style: 'background: #FFF',
                                        height: 20,
                                        renderer: function (storeItem, item) {
                                            this.setTitle(storeItem.get('total'));
                                        }
                                    }
                                }
                            ]
                        }
                    ],
                    tabConfig: {
                        xtype: 'tab',
                        flex: 1,
                        minWidth: 230
                    }
                }
            ]

        });

        me.callParent(arguments);
    }
});