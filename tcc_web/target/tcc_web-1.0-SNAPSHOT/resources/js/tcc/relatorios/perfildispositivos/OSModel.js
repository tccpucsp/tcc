/* global Ext */

Ext.define('resources.js.tcc.relatorios.perfildispositivos.OSModel', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'os'
        },
        {
            name: 'total'
        }
    ]
});
