/* global Ext */

Ext.define('resources.js.tcc.relatorios.acessoanalitico.AAModel', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id'
        },
        {
            name: 'ap_mac_ssid'
        },
        {
            name: 'device_mac'
        },
        {
            name: 'ip'
        },
        {
            name: 'meraki_id'
        },
        {
            name: 'start',
            dateFormat: 'time',
            type: 'date'
        },
        {
            name: 'stop',
            dateFormat: 'time',
            type: 'date'
        }
    ]
});
