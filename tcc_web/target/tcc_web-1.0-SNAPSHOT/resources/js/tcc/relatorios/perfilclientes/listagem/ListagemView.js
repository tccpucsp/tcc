/* global Ext */

Ext.define('resources.js.tcc.relatorios.perfilclientes.listagem.ListagemView', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.relatoriosperfilclienteslistagemview',
    requires: [
        'resources.js.tcc.relatorios.perfilclientes.AmigoModel',
        'resources.js.tcc.relatorios.perfilclientes.GeneroModel',
        'resources.js.tcc.relatorios.perfilclientes.TLModel',
        'resources.js.tcc.relatorios.perfilclientes.FEModel'
    ],
    header: {style: 'background-color: #0277bd;'},
    layout: {
        type: 'fit'
    },
    title: 'Perfil de Clientes',
    iconCls: 'iconProfile',
    itemId: 'pClientes',
    border: true,
    initComponent: function () {
        var me = this;

        var amigoStore = Ext.create('Ext.data.JsonStore', {
            model: 'resources.js.tcc.relatorios.perfilclientes.AmigoModel',
            proxy: {
                type: 'ajax',
                url: 'relatorio/perfilClientesAmigos',
                reader: {
                    type: 'json'
                }
            }
        });

        var tlStore = Ext.create('Ext.data.JsonStore', {
            model: 'resources.js.tcc.relatorios.perfilclientes.TLModel',
            proxy: {
                type: 'ajax',
                url: 'relatorio/perfilClientesTipoLogin',
                reader: {
                    type: 'json'
                }
            }
        });

        var generoStore = Ext.create('Ext.data.JsonStore', {
            model: 'resources.js.tcc.relatorios.perfilclientes.GeneroModel',
            proxy: {
                type: 'ajax',
                url: 'relatorio/perfilClientesGenero',
                reader: {
                    type: 'json'
                }
            }
        });

        var feStore = Ext.create('Ext.data.JsonStore', {
            model: 'resources.js.tcc.relatorios.perfilclientes.FEModel',
            proxy: {
                type: 'ajax',
                url: 'relatorio/perfilClientesFaixaEtaria',
                reader: {
                    type: 'json'
                }
            }
        });

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    title: 'Qtd Amigos Rede Social',
                    layout: {
                        type: 'fit'
                    },
                    items: [
                        {
                            xtype: 'chart',
                            itemId: 'chartAmigo',
                            animate: true,
                            insetPadding: 20,
                            shadow: true,
                            legend: {
                                position: 'right'
                            },
                            theme: 'Category4',
                            store: amigoStore,
                            series: [
                                {
                                    type: 'pie',
                                    showInLegend: true,
                                    angleField: 'total',
                                    tips: {
                                        trackMouse: true,
                                        width: 140,
                                        height: 28,
                                        renderer: function (storeItem, item) {

                                            var total = 0;
                                            amigoStore.each(function (rec) {
                                                total += rec.get('total');
                                            });
                                            this.setTitle(storeItem.get('qtdAmigos') + ': ' + Math.round(storeItem.get('total') / total * 100) + '%');
                                        }
                                    },
                                    highlight: {
                                        segment: {
                                            margin: 20
                                        }
                                    },
                                    label: {
                                        field: 'qtdAmigos',
                                        display: 'rotate',
                                        contrast: true,
                                        font: '15px Arial'
                                    }
                                }
                            ]
                        }
                    ],
                    tabConfig: {
                        xtype: 'tab',
                        flex: 1,
                        minWidth: 230
                    }
                },
                {
                    xtype: 'form',
                    title: 'Gênero',
                    layout: {
                        type: 'fit'
                    },
                    items: [
                        {
                            xtype: 'chart',
                            itemId: 'chartGenero',
                            style: 'background: #fff',
                            animate: true,
                            shadow: false,
                            theme: 'Category2',
                            store: generoStore,
                            insetPadding: 40,
                            axes: [{
                                    title: 'Masculino - Feminino',
                                    type: 'gauge',
                                    position: 'gauge',
                                    steps: 4,
                                    margin: 10,
                                    label: {
                                        renderer: function (v) {
                                            return v;
                                        }
                                    }
                                }],
                            series: [{
                                    type: 'gauge',
                                    field: 'total',
                                    donut: 50
                                }]
                        }
                    ],
                    tabConfig: {
                        xtype: 'tab',
                        flex: 1,
                        minWidth: 230
                    }
                },
                {
                    xtype: 'form',
                    title: 'Faixa Etária',
                    layout: {
                        type: 'fit'
                    },
                    items: [
                        {
                            xtype: 'chart',
                            itemId: 'chartFE',
                            style: 'background: #fff',
                            theme: 'Category1',
                            insetPadding: 40,
                            animate: true,
                            shadow: false,
                            store: feStore,
                            axes: [
                                {
                                    type: 'Numeric',
                                    position: 'left',
                                    fields: ['total'],
                                    label: {
                                        renderer: function (v) {
                                            return v;
                                        }
                                    },
                                    grid: true,
                                    minimum: 0
                                },
                                {
                                    type: 'Category',
                                    position: 'bottom',
                                    fields: ['idade'],
                                    grid: true,
                                    label: {
                                        rotate: {
                                            degrees: -45
                                        }
                                    }
                                }
                            ],
                            series: [
                                {
                                    type: 'column',
                                    axis: 'left',
                                    xField: 'idade',
                                    yField: 'total',
                                    style: {
                                        opacity: 0.80
                                    },
                                    highlight: {
                                        fill: '#000',
                                        'stroke-width': 20,
                                        stroke: '#fff'
                                    },
                                    tips: {
                                        trackMouse: true,
                                        style: 'background: #FFF',
                                        height: 20,
                                        renderer: function (storeItem, item) {
                                            this.setTitle(storeItem.get('idade') + ': ' + storeItem.get('total'));
                                        }
                                    }
                                }
                            ]
                        }
                    ],
                    tabConfig: {
                        xtype: 'tab',
                        flex: 1,
                        minWidth: 230
                    }
                },
                {
                    xtype: 'form',
                    title: 'Tipo de Login',
                    layout: {
                        type: 'fit'
                    },
                    items: [
                        {
                            xtype: 'chart',
                            itemId: 'chartTL',
                            style: 'background: #fff',
                            animate: true,
                            shadow: false,
                            theme: 'Category5',
                            store: tlStore,
                            insetPadding: 40,
                            axes: [
                                {
                                    type: 'Numeric',
                                    position: 'bottom',
                                    fields: ['total'],
                                    label: {
                                        renderer: function (v) {
                                            return v;
                                        }
                                    },
                                    grid: true,
                                    minimum: 0
                                },
                                {
                                    type: 'Category',
                                    position: 'left',
                                    fields: ['tipoLogin'],
                                    grid: true
                                }
                            ],
                            series: [
                                {
                                    type: 'bar',
                                    axis: 'bottom',
                                    xField: 'tipoLogin',
                                    yField: 'total',
                                    style: {
                                        opacity: 0.80
                                    },
                                    highlight: {
                                        fill: '#000',
                                        'stroke-width': 2,
                                        stroke: '#fff'
                                    },
                                    tips: {
                                        trackMouse: true,
                                        style: 'background: #FFF',
                                        height: 20,
                                        renderer: function (storeItem, item) {
                                            this.setTitle(storeItem.get('total'));
                                        }
                                    }
                                }
                            ]
                        }
                    ],
                    tabConfig: {
                        xtype: 'tab',
                        flex: 1,
                        minWidth: 230
                    }
                }
            ]

        });

        me.callParent(arguments);
    }
});