/* global Ext */

Ext.define('resources.js.tcc.relatorios.perfilclientes.GeneroModel', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'genero'
        },
        {
            name: 'total'
        }
    ]
});
