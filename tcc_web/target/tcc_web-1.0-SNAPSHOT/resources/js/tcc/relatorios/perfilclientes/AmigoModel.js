/* global Ext */

Ext.define('resources.js.tcc.relatorios.perfilclientes.AmigoModel', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'qtdAmigos',
            convert: function (value, record) {

                return value + ' amigo(s)';
            }
        },
        {
            name: 'total'
        }
    ]
});
