/* global Ext */

Ext.define('resources.js.tcc.relatorios.perfilclientes.FEModel', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'idade'
        },
        {
            name: 'total'
        }
    ]
});
