/* global Ext */

Ext.define('resources.js.tcc.login.formulario.FormularioSenhaForm', {
    extend: 'Ext.window.Window',
    alias: 'widget.loginformularioformulariosenhaform',
    header: {style: 'background-color: #0277bd;'},
    height: 200,
    width: 380,
    bodyPadding: 10,
    border: true,
    title: 'Gerar Nova Senha',
    iconCls: 'iconPermission',
    url: 'esqueciMinhaSenha',
    initComponent: function () {
        var me = this;

        var tbStyle = resources.js.tcc.utils.Util.getTbFullStyle();

        Ext.applyIf(me, {
            defaults: {
                anchor: '100%',
                labelWidth: 60,
                labelAlign: 'right'
            },
            items: [
                {
                    xtype: 'form',
                    itemId: 'fFormulario',
                    url: 'esqueciMinhaSenha',
                    bodyPadding: 10,
                    border: false,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'displayfield',
                            name: 'aviso',
                            value: 'Será gerada uma nova senha que será exibida na tela. Por favor troque de senha imediatamente.',
                            fieldLabel: 'Atenção',
                            labelStyle: 'font-weight: bold'
                        },
                        {
                            xtype: 'textfield',
                            name: 'login',
                            fieldLabel: 'Usuário',
                            allowBlank: false
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    style: tbStyle,
                    dock: 'bottom',
                    layout: {
                        pack: 'center',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'button',
                            action: 'createNewPassword',
                            iconCls: 'iconPadLock',
                            text: 'Gerar nova senha'
                        }
                    ]
                }
            ]
        });
        me.callParent(arguments);
    }
});