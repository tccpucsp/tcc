/* global Ext */

Ext.define('resources.js.tcc.gerenciadordeprocessos.ProcessoModel', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id'
        },
        {
            name: 'codigo'
        },
        {
            name: 'descricao'
        },
        {
            name: 'periodicidade'
        },
        {
            name: 'ativo',
            convert: function (value, record) {

                if (value === true) {
                    return "Sim";
                } else {
                    return "Não";
                }
            }
        }
    ]
});
