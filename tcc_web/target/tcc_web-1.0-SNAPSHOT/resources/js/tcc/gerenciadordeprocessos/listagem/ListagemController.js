/* global Ext, TCC */

Ext.define('resources.js.tcc.gerenciadordeprocessos.listagem.ListagemController', {
    wListagem: null,
    constructor: function () {
        var me = this;

        var conteudo = TCC.getApplication().mainViewport.queryById('conteudo');

        var view = Ext.create('resources.js.tcc.gerenciadordeprocessos.listagem.ListagemView');

        //limpando container//
        conteudo.removeAll();

        //Inicializando//
        me.wListagem = view;
        me.inicializar();

        //renderizando//
        conteudo.add(view);

    },
    inicializar: function () {
        var me = this;

        var bListagemDetalhes = me.wListagem.queryById('bDetalhes');
        var bListagemExecutar = me.wListagem.queryById('bExecutar');
        var bListagemCancelar = me.wListagem.queryById('bCancelar');
        var bListagemParar = me.wListagem.queryById('bParar');

        var sListagem = me.wListagem.getStore();
        sListagem.load();

        bListagemDetalhes.on('click', function () {
            var validacao = resources.js.tcc.utils.Util.tratarSelecaoGrid(me.wListagem, 'modificar');

            if (validacao.result) {
                var dados = validacao.selection[0];

                Ext.create('resources.js.tcc.gerenciadordeprocessos.formulario.FormularioController', {
                    action: 'modificar',
                    sListagem: sListagem,
                    dados: dados
                });
            }
        }, me);

        me.wListagem.on('celldblclick', function () {
            var validacao = resources.js.tcc.utils.Util.tratarSelecaoGrid(me.wListagem, 'modificar');

            if (validacao.result) {
                var dados = validacao.selection[0];

                Ext.create('resources.js.tcc.gerenciadordeprocessos.formulario.FormularioController', {
                    action: 'modificar',
                    sListagem: sListagem,
                    dados: dados
                });
            }
        }, me);

        bListagemExecutar.on('click', function () {
            var validacao = resources.js.tcc.utils.Util.tratarSelecaoGrid(me.wListagem, 'remover');

            if (validacao.result) {
                Ext.Msg.show({
                    title: 'Confirmação',
                    msg: 'Deseja EXECUTAR o(s) registro(s) selecionado(s)?',
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.Msg.QUESTION,
                    fn: function (btn) {
                        if (btn === 'yes') {
                            var strArrIds = {};
                            strArrIds.ids = [];
                            for (var i = 0; i < validacao.count; i++) {
                                strArrIds.ids[i] = validacao.selection[i].data.id;
                            }
                            me.wListagem.disable();
                            Ext.Ajax.request({
                                url: 'gerenciadorprocesso/executarprocessos',
                                params: strArrIds,
                                callback: function (action, success, response) {
                                    var result = Ext.decode(response.responseText);
                                    me.wListagem.enable();
                                    resources.js.tcc.utils.Util.renderizarResponseMessages(response);
                                    if (result.success) {
                                        sListagem.reload();
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }, me);

        bListagemCancelar.on('click', function () {
            var validacao = resources.js.tcc.utils.Util.tratarSelecaoGrid(me.wListagem, 'remover');

            if (validacao.result) {
                Ext.Msg.show({
                    title: 'Confirmação',
                    msg: 'Deseja CANCELAR o(s) registro(s) selecionado(s)?',
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.Msg.QUESTION,
                    fn: function (btn) {
                        if (btn === 'yes') {
                            var strArrIds = {};
                            strArrIds.ids = [];
                            for (var i = 0; i < validacao.count; i++) {
                                strArrIds.ids[i] = validacao.selection[i].data.id;
                            }
                            me.wListagem.disable();
                            Ext.Ajax.request({
                                url: 'gerenciadorprocesso/cancelarprocessos',
                                params: strArrIds,
                                callback: function (action, success, response) {
                                    var result = Ext.decode(response.responseText);
                                    me.wListagem.enable();
                                    resources.js.tcc.utils.Util.renderizarResponseMessages(response);
                                    if (result.success) {
                                        sListagem.reload();
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }, me);

        bListagemParar.on('click', function () {
            var validacao = resources.js.tcc.utils.Util.tratarSelecaoGrid(me.wListagem, 'remover');

            if (validacao.result) {
                Ext.Msg.show({
                    title: 'Confirmação',
                    msg: 'Deseja PARAR o(s) registro(s) selecionado(s)?',
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.Msg.QUESTION,
                    fn: function (btn) {
                        if (btn === 'yes') {
                            var strArrIds = {};
                            strArrIds.ids = [];
                            for (var i = 0; i < validacao.count; i++) {
                                strArrIds.ids[i] = validacao.selection[i].data.id;
                            }
                            me.wListagem.disable();
                            Ext.Ajax.request({
                                url: 'gerenciadorprocesso/pararprocessos',
                                params: strArrIds,
                                callback: function (action, success, response) {
                                    var result = Ext.decode(response.responseText);
                                    me.wListagem.enable();
                                    resources.js.tcc.utils.Util.renderizarResponseMessages(response);
                                    if (result.success) {
                                        sListagem.reload();
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }, me);
    }
});
