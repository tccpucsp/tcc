/* global Ext */

Ext.define('resources.js.tcc.gerenciadordeprocessos.ProcessoExecucaoLogModel', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id'
        },
        {
            name: 'execucaoId'
        },
        {
            name: 'ProcessoId'
        },
        {
            name: 'log'
        },
        {
            name: 'data',
            type: 'date',
            dateFormat: 'time'
        }
    ]
});
