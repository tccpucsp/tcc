/* global Ext */

Ext.define('resources.js.tcc.gerenciadordeprocessos.ProcessoExecucaoModel', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id'
        },
        {
            name: 'processoId'
        },
        {
            name: 'dataInicio',
            type: 'date',
            dateFormat: 'time'
        },
        {
            name: 'dataFinal',
            type: 'date',
            dateFormat: 'time'
        },
        {
            name: 'situacao'
        }
    ]
});
