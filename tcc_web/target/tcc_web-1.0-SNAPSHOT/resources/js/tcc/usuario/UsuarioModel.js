/* global Ext */

Ext.define('resources.js.tcc.usuario.UsuarioModel', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id'
        },
        {
            name: 'username'
        },
        {
            name: 'name'
        },
        {
            name: 'email'
        }
    ]
});
