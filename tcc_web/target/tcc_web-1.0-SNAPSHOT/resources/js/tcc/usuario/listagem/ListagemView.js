/* global Ext */

Ext.define('resources.js.tcc.usuario.listagem.ListagemView', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.usuariolistagemview',
    requires: [
        'resources.js.tcc.usuario.UsuarioModel'
    ],
    header: {style: 'background-color: #0277bd;'},
    
    layout: {
        type: 'fit'
    },
    title: 'Gerenciamento de Usuários',
    iconCls: 'iconUsers',
    itemId: 'gListagem',
    border: true,
    initComponent: function () {
        var me = this;

        var myStore = Ext.create('Ext.data.JsonStore', {
            model: 'resources.js.tcc.usuario.UsuarioModel',
            proxy: {
                type: 'ajax',
                url: 'usuario/listar',
                reader: {
                    type: 'json'
                }
            }
        });

        me.store = myStore;
        me.selModel = Ext.create('Ext.selection.CheckboxModel', {
        });

        var tbStyle = resources.js.tcc.utils.Util.getTbFullStyle();

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'name',
                    text: 'Nome',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'username',
                    text: 'Username',
                    width: 200
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'email',
                    text: 'E-mail',
                    width: 200
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    style: tbStyle,
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            text: 'Adicionar',
                            itemId: 'bAdicionar',
                            iconCls: 'iconAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Modificar',
                            itemId: 'bModificar',
                            iconCls: 'iconEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Remover',
                            itemId: 'bRemover',
                            iconCls: 'iconRemove'
                        },
                        {
                            xtype: 'tbseparator'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }
});