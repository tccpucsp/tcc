/* global Ext */

Ext.define('resources.js.tcc.usuario.formulario.FormularioView', {
    extend: 'Ext.window.Window',
    height: 220,
    width: 500,
    header: {style: 'background-color: #0277bd;'},
    requires: [
        'resources.js.tcc.utils.emailField'
    ],
    modal: true,
    layout: {
        type: 'fit'
    },
    title: 'Usuário',
    iconCls: 'iconEdit',
    initComponent: function () {
        var me = this;

        var tbStyle = resources.js.tcc.utils.Util.getTbFullStyle();

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    itemId: 'fFormulario',
                    bodyPadding: 10,
                    border: false,
                    defaults: {
                        labelWidth: 60,
                        labelAlign: 'right'
                    },
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'hiddenfield',
                            name: 'id',
                            itemId: 'hId'
                        },
                        {
                            xtype: 'textfield',
                            name: 'name',
                            allowBlank: false,
                            fieldLabel: 'Nome'
                        },
                        {
                            xtype: 'textfield',
                            name: 'username',
                            allowBlank: false,
                            fieldLabel: 'Login'
                        },
                        {
                            xtype: 'container',
                            margin: '0 0 10 0',
                            layout: {
                                type: 'hbox'
                            },
                            defaults: {
                                labelWidth: 60,
                                labelAlign: 'right'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    inputType: 'password',
                                    allowBlank: false,
                                    minLength: 3,
                                    itemId: 'tPass',
                                    name: 'password',
                                    flex: 1,
                                    fieldLabel: 'Senha'
                                },
                                {
                                    xtype: 'progressbar',
                                    margin: '0 5 0 10',
                                    itemId: 'pgSenha',
                                    Text: '',
                                    flex: 1
                                }
                            ]
                        },
                        {
                            xtype: 'emailfield',
                            name: 'email',
                            fieldLabel: 'E-mail',
                            allowBlank: false
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    ui: 'footer',
                    layout: {
                        pack: 'center',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Salvar',
                            itemId: 'bSalvar',
                            iconCls: 'iconSave'
                        },
                        {
                            xtype: 'button',
                            text: 'Sair',
                            itemId: 'bCancelar',
                            iconCls: 'iconSair'
                        }

                    ]
                }
            ]
        });
        me.callParent(arguments);

    }
});