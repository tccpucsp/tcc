/* global Ext */

Ext.Loader.setConfig({
    enabled: true,
    garbageCollect: true,
    scriptChainDelay: true,
    scriptCharset: 'UTF-8'
});

Ext.application({
    name: 'TCC',
    menuController: null,
    loginController: null,
    mainViewport: null,
    launch: function () {
        //definindo padrões//

        //Regex para detecção de aspas em inputs.//
        var txtRegex = new RegExp("[\"]");
        txtRegex.test = function (str) {
            if (str.match(/["]/)) {
                return false;
            } else {
                return true;
            }
        };

        Ext.data.Store.prototype.pageSize = 20;
        Ext.data.reader.Json.prototype.root = 'data';
        Ext.window.Window.prototype.maximizable = true;
        Ext.window.MessageBox.prototype.maximizable = false;
        Ext.toolbar.Toolbar.prototype.ui = 'footer';
        Ext.grid.Panel.prototype.header = {padding: '4 4 4 4'};
        Ext.grid.Panel.prototype.border = false;
        Ext.grid.Panel.prototype.title = '';
        Ext.grid.Panel.prototype.columnLines = true;
        Ext.toolbar.Paging.prototype.dock = 'bottom';
        Ext.toolbar.Paging.prototype.displayInfo = true;
        Ext.form.field.Text.prototype.regex = txtRegex;
        Ext.form.field.Text.prototype.regexText = 'Texto não pode conter o caractere aspas (")';
        Ext.form.field.Number.prototype.hideTrigger = true;
        Ext.form.field.Number.prototype.keyNavEnabled = false;
        Ext.form.field.Number.prototype.mouseWheelEnabled = false;
        Ext.form.field.Number.prototype.spinUpEnabled = false;
        Ext.form.field.Number.prototype.allowExponential = false;
        Ext.form.field.Number.prototype.decimalSeparator = ',';
        Ext.form.field.Date.prototype.format = 'd/m/Y';
        Ext.form.Panel.prototype.title = '';
        Ext.grid.column.Date.prototype.format = 'd/m/Y';
        Ext.grid.column.Number.prototype.align = 'right';
        Ext.button.Button.prototype.enableToggle = false;
        //Ajustes de casas decimais em grids//
        Ext.grid.column.Number.prototype.defaultRenderer = function (a) {
            if (Ext.isNumber(a)) {
                var v = a.toString();
                var l = v.length;
                var d = v.indexOf('.');

                if (d === -1 || v.substr(d + 1, l).length <= 2) {
                    this.format = '0.000,00/i';
                } else {
                    this.format = '0.000,000/i';
                }
            }
            return Ext.util.Format.number(a, this.format);
        };

        //Desabilitando backspace//
        Ext.EventManager.on(Ext.isIE ? document : window, 'keydown', function (e, t) {
            if (e.getKey() === e.BACKSPACE && ((!/^input$/i.test(t.tagName) && !/^textarea$/i.test(t.tagName)) || t.disabled || t.readOnly)) {
                e.stopEvent();
            }
        });

        //criando a tool utility//
        Ext.create('resources.js.tcc.utils.Util');

        //inicializando//
        this.menuController = Ext.create('resources.js.tcc.menu.MenuController');
        this.loginController = Ext.create('resources.js.tcc.login.LoginController', {
            menuController: this.menuController
        });
        this.mainViewport = Ext.create('resources.js.tcc.menu.MainViewport');
        this.mainViewport.show();
    }
});
